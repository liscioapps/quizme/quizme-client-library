export interface Index<T> {
    [name: string]: T;
}
//# sourceMappingURL=IndexHelper.d.ts.map