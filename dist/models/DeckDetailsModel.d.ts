import { BaseEntityModel } from "./BaseEntityModel";
import { CardModel } from "./CardModel";
export interface DeckModel extends BaseEntityModel {
    __id: string;
    title: string;
    description: string;
    userId?: number;
    dueCardCount?: number;
    cards: CardModel[];
}
//# sourceMappingURL=DeckDetailsModel.d.ts.map