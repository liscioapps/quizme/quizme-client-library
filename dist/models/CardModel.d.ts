import { BaseEntityModel } from "./BaseEntityModel";
export interface CardModel extends BaseEntityModel {
    deckId: number;
    questionContent: string;
    questionAnswer: string;
    dueDate?: Date;
}
//# sourceMappingURL=CardModel.d.ts.map