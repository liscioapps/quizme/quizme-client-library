"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubscriptionClaimKeys = void 0;
var SubscriptionClaimKeys;
(function (SubscriptionClaimKeys) {
    SubscriptionClaimKeys["SubscriptionPlanIdClaimKey"] = "SUBSCRIPTION_ID";
    SubscriptionClaimKeys["SubscriptionSettingPrefixClaimKey"] = "SUBSCRIPTION_SETTING";
    SubscriptionClaimKeys["SubscriptionExpiration"] = "SUBSCRIPTION_EXPIRATION";
})(SubscriptionClaimKeys = exports.SubscriptionClaimKeys || (exports.SubscriptionClaimKeys = {}));
//# sourceMappingURL=SubscriptionClaimsKeys.js.map