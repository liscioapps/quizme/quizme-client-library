"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.storageKeys = void 0;
exports.storageKeys = {
    userLoggedin: 'USER_LOGGED_IN',
    authorizationToken: 'AUTHORIZATION_TOKEN',
    refreshToken: 'REFRESH_TOKEN'
};
//# sourceMappingURL=StorageKeys.js.map