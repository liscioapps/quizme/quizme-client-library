export interface BaseEntityModel {
    id?: number;
    createdBy?: string;
    createdDate?: Date;
    modifiedBy?: string;
    modifiedDate?: Date;
}
//# sourceMappingURL=BaseEntityModel.d.ts.map