import { BaseEntityModel } from './BaseEntityModel';
export interface PublicCardModel extends BaseEntityModel {
    publicDeckId: number;
    questionContent: string;
    questionAnswer: string;
}
//# sourceMappingURL=PublicCardModel.d.ts.map