export interface SubscriptionPlanSettings {
    numberOfDecksAllowed: number;
    numberOfCardsAllowed: number;
    assignDecksToOtherUser: boolean;
    analyticsEnabled: boolean;
}
//# sourceMappingURL=PlanSettings.d.ts.map