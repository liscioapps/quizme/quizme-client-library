interface StorageKeys {
    userLoggedin: string;
    authorizationToken: string;
    refreshToken: string;
}
export declare const storageKeys: StorageKeys;
export {};
//# sourceMappingURL=StorageKeys.d.ts.map