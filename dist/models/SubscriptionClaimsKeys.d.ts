export declare enum SubscriptionClaimKeys {
    SubscriptionPlanIdClaimKey = "SUBSCRIPTION_ID",
    SubscriptionSettingPrefixClaimKey = "SUBSCRIPTION_SETTING",
    SubscriptionExpiration = "SUBSCRIPTION_EXPIRATION"
}
//# sourceMappingURL=SubscriptionClaimsKeys.d.ts.map