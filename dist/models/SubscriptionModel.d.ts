export interface UserSubscriptionModel {
    userId: number;
    subscriptionPlanId: number;
    subscriptionPlanName: string;
    subscriptionPlanCode: string;
    appleStoreProductId: string;
    googlePlayProductId: string;
    stripeSubscriptionKey: string;
    expiration: string | null;
}
export interface SubscriptionPlan {
    id: number;
    planName: string;
    subscriptionPlanCode: string;
    planPrice: number;
    currencyUnit: string;
    recurringIntervalValue: number;
    recurringIntervalUnit: 0 | 1 | 2 | 3;
    trialDays: number;
    appleStoreProductId: string;
    googlePlayProductId: string;
    stripeSubscriptionKey: string;
    invalidateOnDate: string | null;
    alternativePlanId: number | null;
    isDeleted: boolean;
    createdDate: string | null;
    createdBy: string;
    modifiedDate: string | null;
    modifiedBy: string;
}
//# sourceMappingURL=SubscriptionModel.d.ts.map