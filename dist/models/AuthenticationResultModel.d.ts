export interface AuthenticationResultModel {
    token: string;
    refreshToken: string;
    forceChangePassword: boolean;
    tokenExpiresAt: number;
}
export interface ResetPasswordModel {
    oldPassword: string;
    newPassword: string;
    confirmPassword: string;
    changePasswordVerificationCode: string;
}
export interface DefaultErrorResult {
    errorMessage: string;
    fullErrorMessage: string;
    errorId: number | null;
    stackTrace: string;
    tenantName: string;
    errorType: string;
}
//# sourceMappingURL=AuthenticationResultModel.d.ts.map