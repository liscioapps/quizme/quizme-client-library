import { BaseEntityModel } from "./BaseEntityModel";
export interface DeckSummaryModel extends BaseEntityModel {
    __id: string;
    title: string;
    description: string;
    userId?: number;
    dueCardCount?: number;
}
//# sourceMappingURL=DeckSummaryModel.d.ts.map