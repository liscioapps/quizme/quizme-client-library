export interface PaginatedResponse<TModel> {
    resultCount: number;
    resultPageIndex: number;
    resultPageSize: number;
    resultTotals: number;
    didReachEnd: boolean;
    data: TModel;
}
//# sourceMappingURL=PaginatedResponse.d.ts.map