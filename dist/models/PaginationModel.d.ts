export interface PaginationModel {
    pageSize: number;
    pageIndex: number;
    orderBy: string;
    orderDirection: "asc" | "desc";
}
//# sourceMappingURL=PaginationModel.d.ts.map