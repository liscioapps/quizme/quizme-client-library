export declare const usePrevious: <T extends unknown>(value: T) => T | undefined;
//# sourceMappingURL=usePrevious.d.ts.map