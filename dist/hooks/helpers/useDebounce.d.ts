export declare const useDebounce: <T>(value: T, delay: number) => T;
//# sourceMappingURL=useDebounce.d.ts.map