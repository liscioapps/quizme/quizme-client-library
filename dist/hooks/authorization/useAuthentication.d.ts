import { AuthenticationResultModel } from "../../models/AuthenticationResultModel";
export declare function useAuthentication(dispatcher?: Function): {
    login: (username: string, password: string) => Promise<AuthenticationResultModel>;
    refreshAuthToken: (timezone: string, refreshToken?: string) => Promise<AuthenticationResultModel>;
    signup: (username: string, password: string) => Promise<AuthenticationResultModel>;
    requestForgotPassword: (userName: string) => Promise<any>;
    requestResetPasswordToken: () => Promise<any>;
    resetPassword: (password: string, resetPasswordToken: string, oldPassword?: string | null) => Promise<any>;
    loadUserInformation: () => Promise<any>;
    logout: (alreadyLoggedOutFromServer?: boolean) => Promise<void>;
};
//# sourceMappingURL=useAuthentication.d.ts.map