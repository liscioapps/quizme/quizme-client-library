import { HomeDashboardStats } from '../../models/HomeDashboardStats';
export declare function useStatsService(): {
    isLoading: boolean;
    getHomepageDashboard: () => Promise<HomeDashboardStats>;
};
//# sourceMappingURL=useStatsService.d.ts.map