import { DeckSummaryModel } from "../../models/DeckSummaryModel";
import { DeckListState } from "../../reducers/deckList.reducer";
export declare function useDeckService(dispatcher?: Function, deckState?: DeckListState): {
    loadingLegacy: boolean;
    /**
     * Retrieves list of decks
     * @param refresh Indicates that the request is to refresh entire list of decks. Set to false to append to current list
     * @param columnsToRetrieve list of properties belong to deck entity to retrieve
     * @param otherFilters Other filters
     * @returns [awaitable] List of decks returned from this request only.
     */
    /**
     * Refreshes the entire decks list
     */
    getDeck: (id: number, columns?: string[]) => Promise<DeckSummaryModel>;
    getDeckDetails: (id: number) => Promise<DeckSummaryModel>;
    createDeck: (deckModel: DeckSummaryModel) => Promise<any>;
    updateDeck: (id: number, model: DeckSummaryModel) => Promise<any>;
    deleteDeck: (id: number) => Promise<any>;
};
//# sourceMappingURL=useDeckService.d.ts.map