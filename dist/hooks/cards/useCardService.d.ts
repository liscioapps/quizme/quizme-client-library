import { DeckListState } from "../../reducers/deckList.reducer";
import { PaginationModel } from "../../models/PaginationModel";
import { CardModel } from "../../models/CardModel";
export declare function useCardService(dispatcher?: Function, deckState?: DeckListState): {
    isLoading: boolean;
    getList: (deckId: number, refresh?: boolean, columnsToRetrieve?: string[], otherFilters?: {}, paginationSettings?: PaginationModel) => Promise<CardModel[]>;
    getCard: (id: number, columns?: string[]) => Promise<CardModel>;
    createCard: (deckModel: CardModel) => Promise<any>;
    updateCard: (id: number, model: CardModel) => Promise<any>;
    deleteCard: (id: number) => Promise<any>;
    reviewCard: (id: number, score: 1 | 2 | 3 | 4 | 5) => Promise<void>;
    getDueCardsByDeck: (deckId: number) => Promise<CardModel[]>;
    getAllDueCardsByUser: () => Promise<CardModel[]>;
};
//# sourceMappingURL=useCardService.d.ts.map