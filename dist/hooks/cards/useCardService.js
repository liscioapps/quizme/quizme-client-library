"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.useCardService = void 0;
var react_1 = require("react");
var deckList_reducer_1 = require("../../reducers/deckList.reducer");
var CardCRUDService_1 = require("../../services/CardCRUDService");
function useCardService(dispatcher, deckState) {
    var dataService = CardCRUDService_1.CardCRUDService.Instance;
    deckState = deckState || deckList_reducer_1.initialDeckState;
    var _a = (0, react_1.useState)(false), loading = _a[0], setLoading = _a[1];
    function getList(deckId, refresh, columnsToRetrieve, otherFilters, paginationSettings) {
        if (refresh === void 0) { refresh = true; }
        if (columnsToRetrieve === void 0) { columnsToRetrieve = []; }
        if (otherFilters === void 0) { otherFilters = {}; }
        if (paginationSettings === void 0) { paginationSettings = deckState === null || deckState === void 0 ? void 0 : deckState.currentPaginationSettings; }
        return __awaiter(this, void 0, void 0, function () {
            var shouldRefresh, pageIndex, decksListResponse;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        setLoading(true);
                        shouldRefresh = refresh || (deckState === null || deckState === void 0 ? void 0 : deckState.decksList.length) === 0;
                        pageIndex = shouldRefresh ? 0 : paginationSettings.pageIndex + 1;
                        return [4 /*yield*/, dataService.getCardsForDeck(deckId, paginationSettings.pageSize, pageIndex, "-ModifiedDate", // hard coding this for now - we don't support changing it
                            columnsToRetrieve, otherFilters)];
                    case 1:
                        decksListResponse = _a.sent();
                        setLoading(false);
                        return [2 /*return*/, decksListResponse];
                }
            });
        });
    }
    function getCard(id, columns) {
        if (columns === void 0) { columns = []; }
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        setLoading(true);
                        return [4 /*yield*/, dataService.getItem(id, columns)];
                    case 1:
                        response = _a.sent();
                        setLoading(false);
                        return [2 /*return*/, response];
                }
            });
        });
    }
    function createCard(deckModel) {
        return __awaiter(this, void 0, void 0, function () {
            var deckCreationResponse;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        setLoading(true);
                        return [4 /*yield*/, dataService.createItem(deckModel)];
                    case 1:
                        deckCreationResponse = _a.sent();
                        setLoading(false);
                        return [2 /*return*/, deckCreationResponse];
                }
            });
        });
    }
    function updateCard(id, model) {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        setLoading(true);
                        return [4 /*yield*/, dataService.updateItem(id, model)];
                    case 1:
                        response = _a.sent();
                        setLoading(false);
                        return [2 /*return*/, response];
                }
            });
        });
    }
    function deleteCard(id) {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        setLoading(true);
                        return [4 /*yield*/, dataService.deleteItem(id)];
                    case 1:
                        response = _a.sent();
                        setLoading(false);
                        return [2 /*return*/, response];
                }
            });
        });
    }
    function reviewCard(id, score) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        setLoading(true);
                        return [4 /*yield*/, dataService.reviewCard(id, score)];
                    case 1:
                        _a.sent();
                        setLoading(false);
                        return [2 /*return*/];
                }
            });
        });
    }
    function getDueCardsByDeck(deckId) {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        setLoading(true);
                        return [4 /*yield*/, dataService.getDueCardsByDeck(deckId)];
                    case 1:
                        response = _a.sent();
                        setLoading(false);
                        return [2 /*return*/, response];
                }
            });
        });
    }
    function getAllDueCardsByUser() {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        setLoading(true);
                        return [4 /*yield*/, dataService.getAllDueCardsByUser()];
                    case 1:
                        response = _a.sent();
                        setLoading(false);
                        return [2 /*return*/, response];
                }
            });
        });
    }
    return {
        isLoading: loading,
        getList: getList,
        getCard: getCard,
        createCard: createCard,
        updateCard: updateCard,
        deleteCard: deleteCard,
        reviewCard: reviewCard,
        getDueCardsByDeck: getDueCardsByDeck,
        getAllDueCardsByUser: getAllDueCardsByUser,
    };
}
exports.useCardService = useCardService;
//# sourceMappingURL=useCardService.js.map