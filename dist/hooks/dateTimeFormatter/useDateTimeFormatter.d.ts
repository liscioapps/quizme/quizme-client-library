export declare function useDateTimeFormatter(): {
    toReadableDateTime(dateTime: Date): string;
    toReadableDayPart(dateTime: Date): string;
    getTimeBetween(date1: Date, date2: Date): string;
};
//# sourceMappingURL=useDateTimeFormatter.d.ts.map