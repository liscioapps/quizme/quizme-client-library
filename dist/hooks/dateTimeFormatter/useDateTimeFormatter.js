"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.useDateTimeFormatter = void 0;
var luxon_1 = require("luxon");
function useDateTimeFormatter() {
    return {
        toReadableDateTime: function (dateTime) {
            return luxon_1.DateTime.fromJSDate(dateTime).toFormat('MM/dd/yyyy tt');
        },
        toReadableDayPart: function (dateTime) {
            return luxon_1.DateTime.fromJSDate(dateTime).toFormat('MM/dd/yyyy');
        },
        getTimeBetween: function (date1, date2) {
            var _a, _b, _c;
            var first = luxon_1.DateTime.fromJSDate(new Date(date1.toDateString()));
            var second = luxon_1.DateTime.fromJSDate(new Date(date2.toDateString()));
            var diff = second
                .diff(first, ['years', 'months', 'days'], {
                conversionAccuracy: 'longterm',
            })
                .toObject();
            var round = function (number) {
                return number < 0 ? Math.ceil(number) : Math.floor(number);
            };
            var days = round((_a = diff.days) !== null && _a !== void 0 ? _a : 0);
            var months = round((_b = diff.months) !== null && _b !== void 0 ? _b : 0);
            var years = round((_c = diff.years) !== null && _c !== void 0 ? _c : 0);
            var format = function (amount, unit) {
                var main = "".concat(Math.abs(amount), " ").concat(unit).concat(Math.abs(amount) > 1 ? 's' : '');
                var withPastTense = amount < 0 ? main + ' ago' : main;
                return withPastTense;
            };
            if (years) {
                return format(years, 'year');
            }
            if (months) {
                return format(months, 'month');
            }
            if (days) {
                return format(days, 'day');
            }
            return 'today';
        },
    };
}
exports.useDateTimeFormatter = useDateTimeFormatter;
//# sourceMappingURL=useDateTimeFormatter.js.map