export declare function useSubscriptionsService(): {
    getUserSubscription: () => Promise<import("../..").UserSubscriptionModel>;
    getStripeCheckoutLink: (stripePriceId: string) => Promise<string>;
    getStripePortalLink: () => Promise<string>;
};
//# sourceMappingURL=useSubscriptionsService.d.ts.map