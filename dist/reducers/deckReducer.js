"use strict";
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.deckReducer = exports.deleteCardFromCurrentDeck = exports.updateCardInCurrentDeck = exports.addCardToCurrentDeck = exports.updateCurrentDeckDescription = exports.clearCurrentDeck = exports.setCurrentDeck = exports.changeOrderByDescending = exports.changeOrderByAsc = exports.appendDecks = exports.setPageSize = exports.setCurrentPageIndex = exports.setDeckList = exports.deckReducerSlice = exports.initialDeckState = void 0;
var toolkit_1 = require("@reduxjs/toolkit");
exports.initialDeckState = {
    decksList: [],
    currentDeck: {
        __id: "",
        title: "",
        description: "",
        cards: [],
    },
    paginationSettings: {
        pageIndex: 0,
        pageSize: 20,
        orderBy: "ModifiedDate",
        orderDirection: "desc",
    },
};
exports.deckReducerSlice = toolkit_1.createSlice({
    name: "Deck",
    initialState: exports.initialDeckState,
    reducers: {
        /**
         * Sets the decks list content of the state
         */
        setDeckList: function (state, action) {
            state.decksList = action.payload.map(function (_) {
                _.__id = toolkit_1.nanoid();
                return _;
            });
        },
        /**
         * Appends decks item to existing list of the state
         */
        appendDecks: function (state, action) {
            var currentDecks = state.decksList;
            currentDecks.push.apply(currentDecks, action.payload);
            state.decksList = currentDecks.map(function (_) {
                _.__id = toolkit_1.nanoid();
                return _;
            });
        },
        /**
         * Sets the current page index of the state
         */
        setCurrentPageIndex: function (state, action) {
            state.paginationSettings.pageIndex = action.payload;
        },
        /**
         * Sets the number of items per page to the state
         */
        setPageSize: function (state, action) {
            state.paginationSettings.pageSize = action.payload;
        },
        changeOrderByAsc: function (state, action) {
            state.paginationSettings.orderDirection = "asc";
            state.paginationSettings.orderBy = action.payload;
        },
        changeOrderByDescending: function (state, action) {
            state.paginationSettings.orderDirection = "desc";
            state.paginationSettings.orderBy = action.payload;
        },
        setCurrentDeck: function (state, action) {
            state.currentDeck = action.payload;
        },
        clearCurrentDeck: function (state) {
            state.currentDeck = exports.initialDeckState.currentDeck;
        },
        updateCurrentDeckDescription: function (state, action) {
            state.currentDeck.title = action.payload.title;
            state.currentDeck.description = action.payload.title;
        },
        addCardToCurrentDeck: function (state, action) {
            state.currentDeck.cards.unshift(action.payload);
            state.currentDeck.dueCardCount = state.currentDeck.dueCardCount
                ? state.currentDeck.dueCardCount + 1
                : 1;
        },
        updateCardInCurrentDeck: function (state, action) {
            state.currentDeck.cards = state.currentDeck.cards.map(function (c) {
                return c.id === action.payload.id ? action.payload : c;
            });
        },
        deleteCardFromCurrentDeck: function (state, action) {
            state.currentDeck.cards = state.currentDeck.cards.filter(function (c) { return c.id !== action.payload; });
        },
    },
});
/**
 * Sets the decks list content of the state
 */
exports.setDeckList = (_a = exports.deckReducerSlice.actions, _a.setDeckList), 
/**
 * Sets the current page index of the state
 */
exports.setCurrentPageIndex = _a.setCurrentPageIndex, 
/**
 * Sets the number of items per page to the state
 */
exports.setPageSize = _a.setPageSize, 
/**
 * Appends decks item to existing list of the state
 */
exports.appendDecks = _a.appendDecks, 
/**
 * Changes the property to sort the items in ascending order
 */
exports.changeOrderByAsc = _a.changeOrderByAsc, 
/**
 * Changes the direction of sorting the items in descending order
 */
exports.changeOrderByDescending = _a.changeOrderByDescending, exports.setCurrentDeck = _a.setCurrentDeck, exports.clearCurrentDeck = _a.clearCurrentDeck, exports.updateCurrentDeckDescription = _a.updateCurrentDeckDescription, exports.addCardToCurrentDeck = _a.addCardToCurrentDeck, exports.updateCardInCurrentDeck = _a.updateCardInCurrentDeck, exports.deleteCardFromCurrentDeck = _a.deleteCardFromCurrentDeck;
exports.deckReducer = exports.deckReducerSlice.reducer;
//# sourceMappingURL=deckReducer.js.map