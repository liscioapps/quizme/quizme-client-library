"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.publicDeckReducer = exports.initialPublicDeckState = void 0;
var immer_1 = require("immer");
exports.initialPublicDeckState = {
    publicDecksList: [],
    currentPaginationSettings: {
        pageIndex: 0,
        pageSize: 2,
        orderBy: 'CreatedDate',
        orderDirection: 'desc',
    },
    searchTerm: '',
    hasMoreDecks: false,
    hasInitialLoad: false,
    isFetching: false,
    pageCurrentlyFetching: 0,
};
exports.publicDeckReducer = (0, immer_1.produce)(function (state, action) {
    var _a;
    // I don't think state should be possibly undefined?
    if (undefined === state) {
        return exports.initialPublicDeckState;
    }
    switch (action.type) {
        case '[PUBLIC_DECKLIST] SET_FETCHING':
            state.isFetching = action.payload;
            break;
        case '[PUBLIC_DECKLIST] SET_HAS_INITIAL_LOAD':
            state.hasInitialLoad = action.payload;
            break;
        case '[PUBLIC_DECKLIST] SET_DECKS':
            state.publicDecksList = action.payload;
            break;
        case '[PUBLIC_DECKLIST] APPEND_DECKS':
            (_a = state.publicDecksList).push.apply(_a, action.payload);
            break;
        case '[PUBLIC_DECKLIST] SET_PAGE_SIZE':
            state.currentPaginationSettings.pageSize = action.payload;
            break;
        case '[PUBLIC_DECKLIST] SET_PAGINATION':
            state.currentPaginationSettings.pageIndex = action.payload.currentPage;
            state.totalDecks = action.payload.totalRecords;
            state.hasMoreDecks = !action.payload.didReachEnd;
            break;
        case '[PUBLIC_DECKLIST] SEARCH_TERM_CHANGED':
            state.searchTerm = action.payload;
            break;
        default:
            return state;
    }
});
//# sourceMappingURL=publicDeckList.reducer.js.map