"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.authReducer = exports.storeUserInformation = exports.setTokenExpired = exports.setTokenExpiresAt = exports.logout = exports.storeRefreshToken = exports.storeAuthToken = exports.authReducerSlice = void 0;
var toolkit_1 = require("@reduxjs/toolkit");
var HttpClientService_1 = require("../services/HttpClientService");
var jwt_decode_1 = __importDefault(require("jwt-decode"));
var SubscriptionClaimsKeys_1 = require("../models/SubscriptionClaimsKeys");
var apiClientService = HttpClientService_1.HttpClientService.Instance;
var initialState = {
    userLoggedIn: false,
    isAdmin: false,
    subscriptionPlanId: 0,
    subscriptionSettings: {
        numberOfDecksAllowed: 0,
        analyticsEnabled: false,
        assignDecksToOtherUser: false,
        numberOfCardsAllowed: 0,
    },
    subscriptionIsExpired: false,
    authorizationToken: null,
    refreshToken: null,
    tokenExpiresAt: null,
    tokenExpired: null,
    userInformation: null,
};
var getPlanSettings = function (decodedToken) {
    var settingsString = decodedToken[SubscriptionClaimsKeys_1.SubscriptionClaimKeys.SubscriptionSettingPrefixClaimKey];
    return JSON.parse(settingsString);
};
var checkSubscriptionIsExpired = function (decodedToken) {
    var unixDateString = decodedToken[SubscriptionClaimsKeys_1.SubscriptionClaimKeys.SubscriptionExpiration];
    var expiration = new Date(Number(unixDateString));
    return expiration < new Date();
};
var checkIsAdmin = function (tokenPayload) {
    var roles = tokenPayload["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"];
    if (typeof roles === "string" && roles === "ADMIN") {
        return true;
    }
    if (Array.isArray(roles) && roles.findIndex(function (r) { return r === "ADMIN"; }) !== -1) {
        return true;
    }
    return false;
};
exports.authReducerSlice = (0, toolkit_1.createSlice)({
    name: "Auth",
    initialState: initialState,
    reducers: {
        storeAuthToken: function (state, action) {
            state.authorizationToken = action.payload;
            if (state.authorizationToken) {
                state.userLoggedIn = true;
                apiClientService.setDefaultHeader("authorization", state.authorizationToken);
                var decodedToken = (0, jwt_decode_1.default)(state.authorizationToken);
                state.isAdmin = checkIsAdmin(decodedToken);
                state.subscriptionPlanId =
                    decodedToken[SubscriptionClaimsKeys_1.SubscriptionClaimKeys.SubscriptionPlanIdClaimKey];
                state.subscriptionSettings = getPlanSettings(decodedToken);
                state.subscriptionIsExpired = checkSubscriptionIsExpired(decodedToken);
            }
            else {
                apiClientService.setDefaultHeader("authorization", null);
            }
        },
        setTokenExpiresAt: function (state, action) {
            state.tokenExpiresAt = action.payload;
        },
        setTokenExpired: function (state, action) {
            state.tokenExpired = action.payload;
        },
        storeRefreshToken: function (state, action) {
            state.refreshToken = action.payload;
        },
        storeUserInformation: function (state, action) {
            state.userInformation = action.payload;
        },
        logout: function (state) {
            state.userLoggedIn = false;
            state.authorizationToken = null;
            state.tokenExpired = true;
            state.tokenExpiresAt = null;
            state.userInformation = null;
        },
    },
});
exports.storeAuthToken = (_a = exports.authReducerSlice.actions, _a.storeAuthToken), exports.storeRefreshToken = _a.storeRefreshToken, exports.logout = _a.logout, exports.setTokenExpiresAt = _a.setTokenExpiresAt, exports.setTokenExpired = _a.setTokenExpired, exports.storeUserInformation = _a.storeUserInformation;
exports.authReducer = exports.authReducerSlice.reducer;
//# sourceMappingURL=authReducer.js.map