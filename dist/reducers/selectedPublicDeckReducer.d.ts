import { Reducer } from "@reduxjs/toolkit";
import { PublicDeckModel } from "../models/PublicDeckModel";
import { SelectedPublicDeckActions } from "../actions/selectedPublicDeck.actions";
export interface SelectedPublicDeckState {
    selectedPublicDeck: PublicDeckModel;
    isFetching: boolean;
    privateDeckId?: number;
    hasError?: boolean;
    errorMessage?: string;
}
export declare const selectedPublicDeckReducer: Reducer<SelectedPublicDeckState, SelectedPublicDeckActions>;
//# sourceMappingURL=selectedPublicDeckReducer.d.ts.map