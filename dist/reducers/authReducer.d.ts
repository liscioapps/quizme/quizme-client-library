import { PayloadAction } from "@reduxjs/toolkit";
import { SubscriptionPlanSettings } from "../models/PlanSettings";
export interface AuthState {
    userLoggedIn: boolean;
    authorizationToken: string | null;
    isAdmin: boolean;
    subscriptionPlanId: number;
    subscriptionSettings: SubscriptionPlanSettings;
    subscriptionIsExpired: boolean;
    refreshToken: string | null;
    tokenExpiresAt: number | null;
    tokenExpired: any;
    userInformation?: any;
}
export declare const authReducerSlice: import("@reduxjs/toolkit").Slice<AuthState, {
    storeAuthToken: (state: AuthState, action: PayloadAction<string>) => void;
    setTokenExpiresAt: (state: AuthState, action: PayloadAction<number>) => void;
    setTokenExpired: (state: AuthState, action: PayloadAction<any>) => void;
    storeRefreshToken: (state: AuthState, action: PayloadAction<string>) => void;
    storeUserInformation: (state: AuthState, action: PayloadAction<any>) => void;
    logout: (state: import("immer/dist/internal").WritableDraft<AuthState>) => void;
}, "Auth">;
export declare const storeAuthToken: import("@reduxjs/toolkit").ActionCreatorWithPayload<string, string>, storeRefreshToken: import("@reduxjs/toolkit").ActionCreatorWithPayload<string, string>, logout: import("@reduxjs/toolkit").ActionCreatorWithoutPayload<string>, setTokenExpiresAt: import("@reduxjs/toolkit").ActionCreatorWithPayload<number, string>, setTokenExpired: import("@reduxjs/toolkit").ActionCreatorWithPayload<any, string>, storeUserInformation: import("@reduxjs/toolkit").ActionCreatorWithPayload<any, string>;
export declare const authReducer: import("redux").Reducer<AuthState, import("redux").AnyAction>;
//# sourceMappingURL=authReducer.d.ts.map