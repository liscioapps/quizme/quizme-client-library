import { Reducer } from '@reduxjs/toolkit';
import { PublicDeckActions } from '../actions/publicDecks.actions';
import { PaginationModel } from '../models';
import { PublicDeckSummaryModel } from '../models/PublicDeckSummaryModel';
export interface PublicDeckListState {
    publicDecksList: PublicDeckSummaryModel[];
    currentPaginationSettings: PaginationModel;
    searchTerm: string;
    totalDecks?: number;
    hasMoreDecks: boolean;
    hasInitialLoad: boolean;
    isFetching: boolean;
    pageCurrentlyFetching: number;
}
export declare const initialPublicDeckState: PublicDeckListState;
export declare const publicDeckReducer: Reducer<PublicDeckListState, PublicDeckActions>;
//# sourceMappingURL=publicDeckList.reducer.d.ts.map