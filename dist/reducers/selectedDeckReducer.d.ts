import { PayloadAction } from "@reduxjs/toolkit";
import { CardModel, DeckModel } from "../models";
export interface SelectedDeckState {
    currentDeck: DeckModel;
}
export declare const initialSelectedDeckState: SelectedDeckState;
export declare const reducer: import("@reduxjs/toolkit").Slice<SelectedDeckState, {
    setCurrentDeck: (state: SelectedDeckState, action: PayloadAction<DeckModel>) => void;
    clearCurrentDeck: (state: SelectedDeckState) => void;
    updateCurrentDeckDescription: (state: SelectedDeckState, action: PayloadAction<{
        title: string;
        description: string;
    }>) => void;
    addCardToCurrentDeck: (state: SelectedDeckState, action: PayloadAction<CardModel>) => void;
    updateCardInCurrentDeck: (state: SelectedDeckState, action: PayloadAction<CardModel>) => void;
    deleteCardFromCurrentDeck: (state: SelectedDeckState, action: PayloadAction<number>) => void;
}, "Selected Deck">;
export declare const setCurrentDeck: import("@reduxjs/toolkit").ActionCreatorWithPayload<DeckModel, string>, clearCurrentDeck: import("@reduxjs/toolkit").ActionCreatorWithoutPayload<string>, updateCurrentDeckDescription: import("@reduxjs/toolkit").ActionCreatorWithPayload<{
    title: string;
    description: string;
}, string>, addCardToCurrentDeck: import("@reduxjs/toolkit").ActionCreatorWithPayload<CardModel, string>, updateCardInCurrentDeck: import("@reduxjs/toolkit").ActionCreatorWithPayload<CardModel, string>, deleteCardFromCurrentDeck: import("@reduxjs/toolkit").ActionCreatorWithPayload<number, string>;
export declare const selectedDeckReducer: import("redux").Reducer<SelectedDeckState, import("redux").AnyAction>;
//# sourceMappingURL=selectedDeckReducer.d.ts.map