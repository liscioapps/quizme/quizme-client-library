import { PayloadAction } from "@reduxjs/toolkit";
import { CardModel } from "../models/CardModel";
export interface ReviewState {
    cardsToReview: CardModel[];
    currentCard: CardModel | null;
}
export declare const initialReviewState: ReviewState;
export declare const reviewReducerSlice: import("@reduxjs/toolkit").Slice<ReviewState, {
    /**
     * Sets the cards the user wants to review
     */
    setCardsToReview: (state: ReviewState, action: PayloadAction<CardModel[]>) => void;
    /**
     * Marks previous card as reviewed, sets the next card to review
     */
    moveToNextCard: (state: ReviewState) => void;
}, "Review">;
export declare const setCardsToReview: import("@reduxjs/toolkit").ActionCreatorWithPayload<CardModel[], string>, moveToNextCard: import("@reduxjs/toolkit").ActionCreatorWithoutPayload<string>;
export declare const reviewReducer: import("redux").Reducer<ReviewState, import("redux").AnyAction>;
//# sourceMappingURL=reviewReducer.d.ts.map