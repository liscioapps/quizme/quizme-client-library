"use strict";
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.selectedDeckReducer = exports.deleteCardFromCurrentDeck = exports.updateCardInCurrentDeck = exports.addCardToCurrentDeck = exports.updateCurrentDeckDescription = exports.clearCurrentDeck = exports.setCurrentDeck = exports.reducer = exports.initialSelectedDeckState = void 0;
var toolkit_1 = require("@reduxjs/toolkit");
exports.initialSelectedDeckState = {
    currentDeck: {
        __id: "",
        title: "",
        description: "",
        cards: [],
    },
};
exports.reducer = (0, toolkit_1.createSlice)({
    name: "Selected Deck",
    initialState: exports.initialSelectedDeckState,
    reducers: {
        setCurrentDeck: function (state, action) {
            state.currentDeck = action.payload;
        },
        clearCurrentDeck: function (state) {
            state.currentDeck = exports.initialSelectedDeckState.currentDeck;
        },
        updateCurrentDeckDescription: function (state, action) {
            state.currentDeck.title = action.payload.title;
            state.currentDeck.description = action.payload.title;
        },
        addCardToCurrentDeck: function (state, action) {
            var _a;
            if ((_a = state.currentDeck.cards) === null || _a === void 0 ? void 0 : _a.length) {
                state.currentDeck.cards.unshift(action.payload);
            }
            else {
                state.currentDeck.cards = [action.payload];
            }
            state.currentDeck.dueCardCount = state.currentDeck.dueCardCount
                ? state.currentDeck.dueCardCount + 1
                : 1;
        },
        updateCardInCurrentDeck: function (state, action) {
            state.currentDeck.cards = state.currentDeck.cards
                ? (state.currentDeck.cards = state.currentDeck.cards.map(function (c) {
                    return c.id === action.payload.id ? action.payload : c;
                }))
                : [action.payload];
        },
        deleteCardFromCurrentDeck: function (state, action) {
            if (state.currentDeck.cards) {
                state.currentDeck.cards = state.currentDeck.cards.filter(function (c) { return c.id !== action.payload; });
            }
        },
    },
});
exports.setCurrentDeck = (_a = exports.reducer.actions, _a.setCurrentDeck), exports.clearCurrentDeck = _a.clearCurrentDeck, exports.updateCurrentDeckDescription = _a.updateCurrentDeckDescription, exports.addCardToCurrentDeck = _a.addCardToCurrentDeck, exports.updateCardInCurrentDeck = _a.updateCardInCurrentDeck, exports.deleteCardFromCurrentDeck = _a.deleteCardFromCurrentDeck;
exports.selectedDeckReducer = exports.reducer.reducer;
//# sourceMappingURL=selectedDeckReducer.js.map