import { PayloadAction } from "@reduxjs/toolkit";
import { CardModel, DeckModel, DeckSummaryModel, PaginationModel } from "../models";
export interface DeckState {
    decksList: DeckSummaryModel[];
    currentDeck: DeckModel;
    paginationSettings: PaginationModel;
}
export declare const initialDeckState: DeckState;
export declare const deckReducerSlice: import("@reduxjs/toolkit").Slice<DeckState, {
    /**
     * Sets the decks list content of the state
     */
    setDeckList: (state: DeckState, action: PayloadAction<DeckSummaryModel[]>) => void;
    /**
     * Appends decks item to existing list of the state
     */
    appendDecks: (state: DeckState, action: PayloadAction<DeckSummaryModel[]>) => void;
    /**
     * Sets the current page index of the state
     */
    setCurrentPageIndex: (state: DeckState, action: PayloadAction<number>) => void;
    /**
     * Sets the number of items per page to the state
     */
    setPageSize: (state: DeckState, action: PayloadAction<number>) => void;
    changeOrderByAsc: (state: DeckState, action: PayloadAction<string>) => void;
    changeOrderByDescending: (state: DeckState, action: PayloadAction<string>) => void;
    setCurrentDeck: (state: DeckState, action: PayloadAction<DeckModel>) => void;
    clearCurrentDeck: (state: DeckState) => void;
    updateCurrentDeckDescription: (state: DeckState, action: PayloadAction<{
        title: string;
        description: string;
    }>) => void;
    addCardToCurrentDeck: (state: DeckState, action: PayloadAction<CardModel>) => void;
    updateCardInCurrentDeck: (state: DeckState, action: PayloadAction<CardModel>) => void;
    deleteCardFromCurrentDeck: (state: DeckState, action: PayloadAction<number>) => void;
}, "Deck">;
export declare const 
/**
 * Sets the decks list content of the state
 */
setDeckList: import("@reduxjs/toolkit").ActionCreatorWithPayload<DeckSummaryModel[], string>, 
/**
 * Sets the current page index of the state
 */
setCurrentPageIndex: import("@reduxjs/toolkit").ActionCreatorWithPayload<number, string>, 
/**
 * Sets the number of items per page to the state
 */
setPageSize: import("@reduxjs/toolkit").ActionCreatorWithPayload<number, string>, 
/**
 * Appends decks item to existing list of the state
 */
appendDecks: import("@reduxjs/toolkit").ActionCreatorWithPayload<DeckSummaryModel[], string>, 
/**
 * Changes the property to sort the items in ascending order
 */
changeOrderByAsc: import("@reduxjs/toolkit").ActionCreatorWithPayload<string, string>, 
/**
 * Changes the direction of sorting the items in descending order
 */
changeOrderByDescending: import("@reduxjs/toolkit").ActionCreatorWithPayload<string, string>, setCurrentDeck: import("@reduxjs/toolkit").ActionCreatorWithPayload<DeckModel, string>, clearCurrentDeck: import("@reduxjs/toolkit").ActionCreatorWithoutPayload<string>, updateCurrentDeckDescription: import("@reduxjs/toolkit").ActionCreatorWithPayload<{
    title: string;
    description: string;
}, string>, addCardToCurrentDeck: import("@reduxjs/toolkit").ActionCreatorWithPayload<CardModel, string>, updateCardInCurrentDeck: import("@reduxjs/toolkit").ActionCreatorWithPayload<CardModel, string>, deleteCardFromCurrentDeck: import("@reduxjs/toolkit").ActionCreatorWithPayload<number, string>;
export declare const deckReducer: import("redux").Reducer<DeckState, import("redux").AnyAction>;
//# sourceMappingURL=deckReducer.d.ts.map