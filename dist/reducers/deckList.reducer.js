"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deckReducer = exports.initialDeckState = void 0;
var immer_1 = require("immer");
exports.initialDeckState = {
    decksList: [],
    currentPaginationSettings: {
        pageIndex: 0,
        pageSize: 30,
        orderBy: 'DueCardCount',
        orderDirection: 'desc',
    },
    searchTerm: '',
    hasMoreDecks: false,
    hasInitialLoad: false,
    isFetching: false,
    pageCurrentlyFetching: 0,
};
exports.deckReducer = (0, immer_1.produce)(function (state, action) {
    var _a;
    // I don't think state should be possibly undefined?
    if (undefined === state) {
        return exports.initialDeckState;
    }
    switch (action.type) {
        case '[DECKLIST] SET_FETCHING':
            state.isFetching = action.payload;
            break;
        case '[DECKLIST] SET_HAS_INITIAL_LOAD':
            state.hasInitialLoad = action.payload;
            break;
        case '[DECKLIST] SET_DECKS':
            state.decksList = action.payload;
            break;
        case '[DECKLIST] APPEND_DECKS':
            (_a = state.decksList).push.apply(_a, action.payload);
            break;
        case '[DECKLIST] SET_ORDER_BY_ASCENDING':
            state.currentPaginationSettings.orderBy = action.payload.field;
            state.currentPaginationSettings.orderDirection = 'asc';
            break;
        case '[DECKLIST] SET_ORDER_BY_DESCENDING':
            state.currentPaginationSettings.orderBy = action.payload.field;
            state.currentPaginationSettings.orderDirection = 'desc';
            break;
        case '[DECKLIST] SET_PAGE_SIZE':
            state.currentPaginationSettings.pageSize = action.payload;
            break;
        case '[DECKLIST] SET_PAGINATION':
            state.currentPaginationSettings.pageIndex = action.payload.currentPage;
            state.totalDecks = action.payload.totalRecords;
            state.hasMoreDecks = !action.payload.didReachEnd;
            break;
        case '[DECKLIST] SORT_PROPERTIES_CHANGED':
            state.currentPaginationSettings.orderBy = action.payload.field;
            state.currentPaginationSettings.orderDirection = action.payload.direction;
            break;
        case '[DECKLIST] SEARCH_TERM_CHANGED':
            state.searchTerm = action.payload;
            break;
        default:
            return state;
    }
});
//# sourceMappingURL=deckList.reducer.js.map