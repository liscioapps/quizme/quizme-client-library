import { Reducer } from '@reduxjs/toolkit';
import { DeckActions } from '../actions/decks.actions';
import { DeckSummaryModel, PaginationModel } from '../models';
export interface DeckListState {
    decksList: DeckSummaryModel[];
    currentPaginationSettings: PaginationModel;
    searchTerm: string;
    totalDecks?: number;
    hasMoreDecks: boolean;
    hasInitialLoad: boolean;
    isFetching: boolean;
    pageCurrentlyFetching: number;
}
export declare const initialDeckState: DeckListState;
export declare const deckReducer: Reducer<DeckListState, DeckActions>;
//# sourceMappingURL=deckList.reducer.d.ts.map