"use strict";
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.reviewReducer = exports.moveToNextCard = exports.setCardsToReview = exports.reviewReducerSlice = exports.initialReviewState = void 0;
var toolkit_1 = require("@reduxjs/toolkit");
exports.initialReviewState = {
    cardsToReview: [],
    currentCard: null,
};
exports.reviewReducerSlice = (0, toolkit_1.createSlice)({
    name: "Review",
    initialState: exports.initialReviewState,
    reducers: {
        /**
         * Sets the cards the user wants to review
         */
        setCardsToReview: function (state, action) {
            var _a;
            state.cardsToReview = action.payload;
            state.currentCard = (_a = action.payload) === null || _a === void 0 ? void 0 : _a[0];
        },
        /**
         * Marks previous card as reviewed, sets the next card to review
         */
        moveToNextCard: function (state) {
            state.cardsToReview.shift();
            state.currentCard = state.cardsToReview.length
                ? state.cardsToReview[0]
                : null;
        },
    },
});
exports.setCardsToReview = (_a = exports.reviewReducerSlice.actions, _a.setCardsToReview), exports.moveToNextCard = _a.moveToNextCard;
exports.reviewReducer = exports.reviewReducerSlice.reducer;
//# sourceMappingURL=reviewReducer.js.map