"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.selectedPublicDeckReducer = void 0;
var immer_1 = require("immer");
var initialSelectedPublicDeckState = {
    selectedPublicDeck: {
        description: "",
        title: "",
        approved: false,
        cards: [],
    },
    isFetching: false,
};
exports.selectedPublicDeckReducer = (0, immer_1.produce)(function (state, action) {
    // I don't think state should be possibly undefined?
    if (undefined === state) {
        return initialSelectedPublicDeckState;
    }
    switch (action.type) {
        case "[SELECTED_PUBLIC_DECK] SET_DECK":
            state.selectedPublicDeck = action.payload.deck;
            break;
        case "[SELECTED_PUBLIC_DECK] SET_IS_FETCHING":
            state.isFetching = action.payload.isFetching;
            break;
        case "[SELECTED_PUBLIC_DECK] SET_ERROR":
            state.hasError = true;
            state.errorMessage = action.payload.errorMessage;
            break;
        case "[SELECTED_PUBLIC_DECK] CLEAR_ERROR":
            state.hasError = false;
            state.errorMessage = "";
            break;
        case "[SELECTED_PUBLIC_DECK] SET_PRIVATE_DECK_ID":
            state.privateDeckId = action.payload.privateDeckId;
            break;
        case "[SELECTED_PUBLIC_DECK] SET APPROVED":
            state.selectedPublicDeck.approved = action.payload.approved;
            break;
        default:
            return state;
    }
});
//# sourceMappingURL=selectedPublicDeckReducer.js.map