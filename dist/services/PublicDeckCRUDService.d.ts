import { BaseCRUDApiClient } from './BaseCRUDApiClient';
import { PublicDeckModel } from '../models/PublicDeckModel';
export declare class PublicDeckCRUDService extends BaseCRUDApiClient<PublicDeckModel> {
    protected baseURL: string;
    private static _instance;
    static get Instance(): PublicDeckCRUDService;
    addToMyDecks(publicDeckId: number): Promise<number>;
    requestPublish(deckId: number): Promise<number>;
    approveDeck(publicDeckId: number): Promise<number>;
    unapproveDeck(publicDeckId: number): Promise<number>;
}
//# sourceMappingURL=PublicDeckCRUDService.d.ts.map