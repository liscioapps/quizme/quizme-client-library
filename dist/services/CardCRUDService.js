"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CardCRUDService = void 0;
var config_1 = require("./../config");
var BaseCRUDApiClient_1 = require("./BaseCRUDApiClient");
var CardCRUDService = /** @class */ (function (_super) {
    __extends(CardCRUDService, _super);
    function CardCRUDService() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.baseURL = "api/Cards";
        return _this;
    }
    Object.defineProperty(CardCRUDService, "Instance", {
        get: function () {
            return (CardCRUDService._instance ||
                (CardCRUDService._instance = new CardCRUDService()));
        },
        enumerable: false,
        configurable: true
    });
    CardCRUDService.prototype.getCardsForDeck = function (deckId, pageSize, pageIndex, orderBy, columns, otherFilters) {
        if (pageSize === void 0) { pageSize = 20; }
        if (pageIndex === void 0) { pageIndex = 0; }
        if (orderBy === void 0) { orderBy = ""; }
        if (columns === void 0) { columns = []; }
        if (otherFilters === void 0) { otherFilters = null; }
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.httpClient
                            .get("".concat(config_1.quizMeConfig.apiEndpointUrl, "/api/Decks/").concat(deckId, "/cards"), {
                            headers: {
                                Accept: "application/json",
                            },
                            params: __assign({ pageIndex: pageIndex, pageSize: pageSize, orderBy: orderBy || null, columns: columns.length ? columns.join(",") : null }, (otherFilters || {})),
                        })
                            .then(function (_) { return _.data; })];
                    case 1:
                        response = _a.sent();
                        return [2 /*return*/, response];
                }
            });
        });
    };
    CardCRUDService.prototype.reviewCard = function (cardId, score) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.httpClient.post("".concat(config_1.quizMeConfig.apiEndpointUrl, "/api/Answers"), { cardId: cardId, answer: score }, {
                            headers: {
                                Accept: "application/json",
                            },
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    CardCRUDService.prototype.getDueCardsByDeck = function (deckId) {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.httpClient
                            .get("".concat(config_1.quizMeConfig.apiEndpointUrl, "/api/Decks/").concat(deckId, "/dueCards"), {
                            headers: {
                                Accept: "application/json",
                            },
                        })
                            .then(function (_) { return _.data; })];
                    case 1:
                        response = _a.sent();
                        return [2 /*return*/, response];
                }
            });
        });
    };
    CardCRUDService.prototype.getAllDueCardsByUser = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.httpClient
                            .get("".concat(config_1.quizMeConfig.apiEndpointUrl, "/api/cards/due"), {
                            headers: {
                                Accept: "application/json",
                            },
                        })
                            .then(function (_) { return _.data; })];
                    case 1:
                        response = _a.sent();
                        return [2 /*return*/, response];
                }
            });
        });
    };
    return CardCRUDService;
}(BaseCRUDApiClient_1.BaseCRUDApiClient));
exports.CardCRUDService = CardCRUDService;
//# sourceMappingURL=CardCRUDService.js.map