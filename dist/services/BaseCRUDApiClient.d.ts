import HttpClientService from './HttpClientService';
import { BaseStorageService } from './BaseStorageService';
import { PaginatedResponse } from '../models/PaginatedResponse';
export declare abstract class BaseCRUDApiClient<TModel> {
    protected httpClient: HttpClientService;
    protected get storageService(): BaseStorageService | any;
    protected abstract baseURL: string;
    constructor();
    /**
     * Retrieves list of items with pagination support
     * @param pageSize number of items to retrieve in the request
     * @param pageIndex the index of the page to return
     * @param orderBy specifies the property to sort the items before return
     * @param columns the properties list of the entity to return
     * @param otherFilters other filters
     * @returns List of items
     */
    getList(pageSize?: number, pageIndex?: number, orderBy?: string, columns?: string[], otherFilters?: any): Promise<PaginatedResponse<TModel[]>>;
    getItem(itemId: number, columns?: string[]): Promise<TModel>;
    createItem(item: TModel): Promise<any>;
    updateItem(itemId: number, item: Partial<TModel>): Promise<any>;
    deleteItem(itemId: number): Promise<any>;
}
//# sourceMappingURL=BaseCRUDApiClient.d.ts.map