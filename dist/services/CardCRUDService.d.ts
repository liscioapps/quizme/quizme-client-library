import { CardModel } from "../models/CardModel";
import { BaseCRUDApiClient } from "./BaseCRUDApiClient";
export declare class CardCRUDService extends BaseCRUDApiClient<CardModel> {
    protected baseURL: string;
    private static _instance;
    static get Instance(): CardCRUDService;
    getCardsForDeck(deckId: number, pageSize?: number, pageIndex?: number, orderBy?: string, columns?: string[], otherFilters?: any): Promise<CardModel[]>;
    reviewCard(cardId: number, score: 1 | 2 | 3 | 4 | 5): Promise<void>;
    getDueCardsByDeck(deckId: number): Promise<CardModel[]>;
    getAllDueCardsByUser(): Promise<CardModel[]>;
}
//# sourceMappingURL=CardCRUDService.d.ts.map