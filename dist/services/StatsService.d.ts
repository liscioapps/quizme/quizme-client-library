import { HomeDashboardStats } from '../models/HomeDashboardStats';
import HttpClientService from './HttpClientService';
export declare class StatsService {
    protected httpClient: HttpClientService;
    protected baseURL: string;
    private static _instance;
    static get Instance(): StatsService;
    constructor();
    getHomepageDashboard(): Promise<HomeDashboardStats>;
}
//# sourceMappingURL=StatsService.d.ts.map