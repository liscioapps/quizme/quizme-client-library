"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeckCRUDService = void 0;
var BaseCRUDApiClient_1 = require("./BaseCRUDApiClient");
var DeckCRUDService = /** @class */ (function (_super) {
    __extends(DeckCRUDService, _super);
    /**
     *
     */
    function DeckCRUDService() {
        var _this = _super.call(this) || this;
        _this.baseURL = 'api/Decks';
        return _this;
    }
    Object.defineProperty(DeckCRUDService, "Instance", {
        get: function () {
            return DeckCRUDService._instance || (DeckCRUDService._instance = new DeckCRUDService());
        },
        enumerable: false,
        configurable: true
    });
    return DeckCRUDService;
}(BaseCRUDApiClient_1.BaseCRUDApiClient));
exports.DeckCRUDService = DeckCRUDService;
//# sourceMappingURL=DeckService.js.map