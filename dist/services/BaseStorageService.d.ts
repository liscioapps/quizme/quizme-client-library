export interface BaseStorageService {
    store(key: string, value: any): Promise<any>;
    retrieve(key: string): Promise<any>;
    contains(key: string): Promise<boolean>;
    remove(key: string): Promise<any>;
    clear(): Promise<any>;
}
export declare class LocalforageStorageService implements BaseStorageService {
    static _instance: LocalforageStorageService;
    static get Instance(): LocalforageStorageService;
    private storage;
    constructor();
    contains(key: string): Promise<boolean>;
    retrieve(key: string): Promise<any>;
    store(key: string, value: any): Promise<any>;
    remove(key: string): Promise<any>;
    clear(): Promise<any>;
}
//# sourceMappingURL=BaseStorageService.d.ts.map