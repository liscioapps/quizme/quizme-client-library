import { DeckModel } from "../models/DeckSummaryModel";
import { BaseCRUDApiClient } from "./BaseCRUDApiClient";
export declare class DeckCRUDService extends BaseCRUDApiClient<DeckModel> {
  protected baseURL: string;
  private static _instance;
  static get Instance(): DeckCRUDService;
  /**
   *
   */
  constructor();
}
//# sourceMappingURL=DeckService.d.ts.map
