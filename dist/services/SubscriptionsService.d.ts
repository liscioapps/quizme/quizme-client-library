import { UserSubscriptionModel } from "../models/SubscriptionModel";
import HttpClientService from "./HttpClientService";
export declare class SubscriptionsService {
    protected httpClient: HttpClientService;
    protected baseURL: string;
    private static _instance;
    static get Instance(): SubscriptionsService;
    constructor();
    getUserSubscription(): Promise<UserSubscriptionModel>;
    getStripeCheckoutLink(stripePriceId: string): Promise<string>;
    getStripePortalLink(): Promise<string>;
}
//# sourceMappingURL=SubscriptionsService.d.ts.map