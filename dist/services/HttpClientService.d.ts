import { AxiosRequestConfig, AxiosResponse } from 'axios';
export interface ExtendedAxiosRequestConfig extends AxiosRequestConfig {
    /**
     * Tells HTTP Clients not to include the Authorization header
     */
    ignoreAuthToken?: boolean;
}
export declare class HttpClientService {
    static _instance: HttpClientService;
    _apiClient: import("axios").AxiosInstance;
    defaultHeaders: any;
    static get Instance(): HttpClientService;
    constructor();
    setDefaultHeader(name: string, value: any): void;
    removeDefaultHeader(name: string): void;
    registerRequestInterceptor(...interceptors: any[]): void;
    registerResponseInterceptors(...interceptors: any[]): Promise<void>;
    /**
     * Make GET request to the given URL
     * @param url The URL to make request
     * @param config the options of the request
     * @returns
     */
    get<ModelType>(url: string, config?: ExtendedAxiosRequestConfig | undefined): Promise<AxiosResponse<ModelType>>;
    /**
     * Make POST request to the given URL, optionally include the data
     * @param url The URL to make request
     * @param data Data to include in the POST request
     * @param config the options of the request
     * @returns
     */
    post<ModelType>(url: string, data?: any, config?: ExtendedAxiosRequestConfig | undefined): Promise<AxiosResponse<ModelType>>;
    /**
     * Make PUT request to the given URL, optionally include the data
     * @param url The URL to make request
     * @param data Data to include in the POST request
     * @param config the options of the request
     * @returns
     */
    put<ModelType>(url: string, data?: any, config?: ExtendedAxiosRequestConfig | undefined): Promise<AxiosResponse<ModelType>>;
    /**
     * Make DELETE request to the given URL
     * @param url The URL to make request
     * @param config the options of the request
     * @returns
     */
    delete<ModelType>(url: string, config?: ExtendedAxiosRequestConfig | undefined): Promise<AxiosResponse<ModelType>>;
}
export default HttpClientService;
//# sourceMappingURL=HttpClientService.d.ts.map