import { AuthenticationResultModel, ResetPasswordModel } from "../models/AuthenticationResultModel";
export declare class AuthService {
    private httpClient;
    private get storageService();
    private static _instance;
    static get Instance(): AuthService;
    constructor();
    isLoggedIn(): Promise<boolean>;
    signUp(signUpUserModel: any): Promise<AuthenticationResultModel>;
    requestResetPassword(userEmail: string): Promise<any>;
    requestResetPasswordToken(): Promise<ResetPasswordModel>;
    resetPassword(resetPasswordModel: ResetPasswordModel): Promise<any>;
    hasRefreshToken(): Promise<boolean>;
    refreshAuthToken(timezone: string, refreshToken?: string): Promise<AuthenticationResultModel>;
    logIn(userName: string, password: string): Promise<AuthenticationResultModel>;
    logout(alreadyLoggedOutFromServer?: boolean): Promise<void>;
    loadUserProfile(): Promise<any>;
}
export default AuthService;
//# sourceMappingURL=AuthService.d.ts.map