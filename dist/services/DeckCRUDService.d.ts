import { DeckSummaryModel } from "../models/DeckSummaryModel";
import { BaseCRUDApiClient } from "./BaseCRUDApiClient";
export declare class DeckCRUDService extends BaseCRUDApiClient<DeckSummaryModel> {
    protected baseURL: string;
    private static _instance;
    static get Instance(): DeckCRUDService;
    getDetails(itemId: number): Promise<DeckSummaryModel>;
}
//# sourceMappingURL=DeckCRUDService.d.ts.map