"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
var HttpClientService_1 = __importDefault(require("./HttpClientService"));
var config_1 = require("../config");
var StorageKeys_1 = require("../models/StorageKeys");
var luxon_1 = require("luxon");
var AuthService = /** @class */ (function () {
    function AuthService() {
        if (undefined === config_1.quizMeConfig.storageService ||
            null === config_1.quizMeConfig.storageService) {
            throw new Error("quizMeConfig.storageService must be registered before application starts");
        }
        this.httpClient = HttpClientService_1.default.Instance;
    }
    Object.defineProperty(AuthService.prototype, "storageService", {
        get: function () {
            return config_1.quizMeConfig.storageService;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(AuthService, "Instance", {
        get: function () {
            return AuthService._instance || (AuthService._instance = new AuthService());
        },
        enumerable: false,
        configurable: true
    });
    AuthService.prototype.isLoggedIn = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.storageService.contains(StorageKeys_1.storageKeys.authorizationToken)];
            });
        });
    };
    AuthService.prototype.signUp = function (signUpUserModel) {
        return __awaiter(this, void 0, void 0, function () {
            var response, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        return [4 /*yield*/, this.httpClient
                                .post("".concat(config_1.quizMeConfig.apiEndpointUrl, "/api/SignUp/register"), __assign({ registerFromMobileApp: true }, signUpUserModel), {
                                headers: {
                                    Accept: "application/json",
                                },
                            })
                                .then(function (_) { return _.data; })];
                    case 1:
                        response = _a.sent();
                        return [4 /*yield*/, this.storageService.store(StorageKeys_1.storageKeys.refreshToken, response.refreshToken)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, response];
                    case 3:
                        error_1 = _a.sent();
                        throw {
                            status: error_1.response.status,
                            data: error_1.response.data,
                            response: error_1.response,
                        };
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    AuthService.prototype.requestResetPassword = function (userEmail) {
        return __awaiter(this, void 0, void 0, function () {
            var response, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.httpClient
                                .post("".concat(config_1.quizMeConfig.apiEndpointUrl, "/api/userPassword/requestForgotPasswordEmail"), {
                                userEmail: userEmail,
                            }, {
                                headers: {
                                    Accept: "application/json",
                                },
                            })
                                .then(function (_) { return _.data; })];
                    case 1:
                        response = _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_2 = _a.sent();
                        throw {
                            status: error_2.response.status,
                            data: error_2.response.data,
                            response: error_2.response,
                        };
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AuthService.prototype.requestResetPasswordToken = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, error_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.httpClient
                                .post("".concat(config_1.quizMeConfig.apiEndpointUrl, "/api/userPassword/requestChangePassword"), null, {
                                headers: {
                                    Accept: "application/json",
                                },
                            })
                                .then(function (_) { return _.data; })];
                    case 1:
                        response = _a.sent();
                        return [2 /*return*/, response];
                    case 2:
                        error_3 = _a.sent();
                        throw {
                            status: error_3.response.status,
                            data: error_3.response.data,
                            response: error_3.response,
                        };
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AuthService.prototype.resetPassword = function (resetPasswordModel) {
        return __awaiter(this, void 0, void 0, function () {
            var response, error_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.httpClient
                                .post("".concat(config_1.quizMeConfig.apiEndpointUrl, "/api/userPassword/resetPassword"), __assign({}, resetPasswordModel), {
                                headers: {
                                    Accept: "application/json",
                                },
                            })
                                .then(function (_) { return _.data; })];
                    case 1:
                        response = _a.sent();
                        return [2 /*return*/, response];
                    case 2:
                        error_4 = _a.sent();
                        throw {
                            status: error_4.response.status,
                            data: error_4.response.data,
                            response: error_4.response,
                        };
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AuthService.prototype.hasRefreshToken = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.storageService.contains(StorageKeys_1.storageKeys.refreshToken)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    AuthService.prototype.refreshAuthToken = function (timezone, refreshToken) {
        return __awaiter(this, void 0, void 0, function () {
            var response, error_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!!refreshToken) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.storageService.retrieve(StorageKeys_1.storageKeys.refreshToken)];
                    case 1:
                        refreshToken = _a.sent();
                        _a.label = 2;
                    case 2:
                        if (!refreshToken) {
                            throw {
                                status: 401,
                            };
                        }
                        _a.label = 3;
                    case 3:
                        _a.trys.push([3, 6, , 9]);
                        return [4 /*yield*/, this.httpClient
                                .post("".concat(config_1.quizMeConfig.apiEndpointUrl, "/api/Auth/refreshToken"), null, {
                                headers: {
                                    "X-REFRESH-TOKEN": refreshToken,
                                    "X-CLIENT-TIMEZONE": timezone,
                                },
                                ignoreAuthToken: true,
                            })
                                .then(function (_) { return _.data; })];
                    case 4:
                        response = _a.sent();
                        return [4 /*yield*/, this.storageService.store(StorageKeys_1.storageKeys.refreshToken, response.refreshToken)];
                    case 5:
                        _a.sent();
                        return [2 /*return*/, response];
                    case 6:
                        error_5 = _a.sent();
                        if (!(error_5.response.status === 401)) return [3 /*break*/, 8];
                        return [4 /*yield*/, this.storageService.remove(StorageKeys_1.storageKeys.refreshToken)];
                    case 7:
                        _a.sent();
                        _a.label = 8;
                    case 8: throw {
                        status: error_5.response.status,
                        data: error_5.response.data,
                        response: error_5.response,
                    };
                    case 9: return [2 /*return*/];
                }
            });
        });
    };
    AuthService.prototype.logIn = function (userName, password) {
        return __awaiter(this, void 0, void 0, function () {
            var response, error_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        return [4 /*yield*/, this.httpClient
                                .post("".concat(config_1.quizMeConfig.apiEndpointUrl, "/api/Auth/login"), {
                                userName: userName.toLocaleLowerCase(),
                                password: password,
                                rememberMe: true,
                            }, {
                                headers: {
                                    Accept: "application/json",
                                    "X-CLIENT-TIMEZONE": luxon_1.DateTime.now().zoneName,
                                },
                            })
                                .then(function (_) { return _.data; })];
                    case 1:
                        response = _a.sent();
                        return [4 /*yield*/, this.storageService.store(StorageKeys_1.storageKeys.refreshToken, response.refreshToken)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, response];
                    case 3:
                        error_6 = _a.sent();
                        console.log(JSON.stringify(error_6));
                        if (!error_6.response) {
                            throw error_6;
                        }
                        throw {
                            status: error_6.response.status,
                            data: error_6.response.data,
                            response: error_6.response,
                        };
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    AuthService.prototype.logout = function (alreadyLoggedOutFromServer) {
        if (alreadyLoggedOutFromServer === void 0) { alreadyLoggedOutFromServer = false; }
        return __awaiter(this, void 0, void 0, function () {
            var response, error_7;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!!alreadyLoggedOutFromServer) return [3 /*break*/, 6];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 6]);
                        return [4 /*yield*/, this.httpClient
                                .post("".concat(config_1.quizMeConfig.apiEndpointUrl, "/api/Auth/logout"))
                                .then(function (_) { return _.data; })];
                    case 2:
                        response = _a.sent();
                        return [3 /*break*/, 6];
                    case 3:
                        error_7 = _a.sent();
                        if (!(error_7.response.status === 401)) return [3 /*break*/, 5];
                        return [4 /*yield*/, this.storageService.clear()];
                    case 4:
                        _a.sent();
                        return [2 /*return*/];
                    case 5: throw {
                        status: error_7.response.status,
                        data: error_7.response.data,
                        response: error_7.response,
                    };
                    case 6: return [4 /*yield*/, this.storageService.clear()];
                    case 7:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AuthService.prototype.loadUserProfile = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, error_8;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.httpClient
                                .get("".concat(config_1.quizMeConfig.apiEndpointUrl, "/api/Auth/profile"))
                                .then(function (_) { return _.data; })];
                    case 1:
                        response = _a.sent();
                        return [2 /*return*/, response];
                    case 2:
                        error_8 = _a.sent();
                        throw {
                            status: error_8.response && error_8.response.status,
                            data: error_8.response && error_8.response.data,
                            response: error_8.response,
                        };
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    return AuthService;
}());
exports.AuthService = AuthService;
exports.default = AuthService;
//# sourceMappingURL=AuthService.js.map