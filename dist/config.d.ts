import { BaseStorageService } from './services/BaseStorageService';
export interface QuizMeConfig {
    /**
     * The Endpoint URL to API service
     */
    apiEndpointUrl: string;
    /**
     * The service that is used for handling storage
     */
    storageService?: BaseStorageService;
    /**
     * Tells the authentication service to check for the validity of refresh token after the given time in seconds
     */
    checkAndRefreshTokenPeriodically: number | null;
}
export declare const quizMeConfig: QuizMeConfig;
//# sourceMappingURL=config.d.ts.map