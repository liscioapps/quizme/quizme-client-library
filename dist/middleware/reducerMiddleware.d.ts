import { AnyAction, Dispatch, Middleware } from 'redux';
export interface ReducerMiddleware<TState, TAction extends AnyAction> {
    (getPartialState: () => TState, next: Dispatch<AnyAction>, dispatch: Dispatch<AnyAction>, action: TAction): void;
}
export declare function createMiddleWare<TState, TPartialState>(reducerMiddleWare: ReducerMiddleware<TPartialState, any>, selector: (state: TState) => TPartialState): Middleware<{}, TState>;
//# sourceMappingURL=reducerMiddleware.d.ts.map