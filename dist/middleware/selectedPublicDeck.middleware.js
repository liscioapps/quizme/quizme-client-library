"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.selectedPublicDeckMiddleware = void 0;
var selectedPublicDeck_actions_1 = require("../actions/selectedPublicDeck.actions");
var services_1 = require("../services");
var selectedPublicDeckMiddleware = function (getState, dispatch, next, action) {
    console.log("dispatch: ", action);
    next(action);
    var publicDecksService = services_1.PublicDeckCRUDService.Instance;
    if (action.type === "[SELECTED_PUBLIC_DECK] FETCH_PUBLIC_DECK") {
        var state = getState();
        dispatch((0, selectedPublicDeck_actions_1.SetSelectedPublicDeckIsFetching)(true));
        publicDecksService
            .getItem(action.payload.publicDeckId)
            .then(function (response) {
            dispatch((0, selectedPublicDeck_actions_1.fetchSelectedPublicDeckSucceeded)(response)); // initial fetch success
        })
            .catch(function (e) {
            dispatch((0, selectedPublicDeck_actions_1.fetchSelectedPublicDeckFailed)(e.message));
        });
    }
    if (action.type === "[SELECTED_PUBLIC_DECK] FETCH_DECK_FAILURE") {
        var state = getState();
        if (state.isFetching) {
            // one request cycle at a time
            return;
        }
        dispatch((0, selectedPublicDeck_actions_1.setSelectedPublicDeckError)(action.payload.errorMsg));
        dispatch((0, selectedPublicDeck_actions_1.SetSelectedPublicDeckIsFetching)(false));
    }
    if (action.type === "[SELECTED_PUBLIC_DECK] FETCH_SUCCEDED") {
        var state = getState();
        dispatch((0, selectedPublicDeck_actions_1.setSelectedPublicDeck)(action.payload));
        dispatch((0, selectedPublicDeck_actions_1.SetSelectedPublicDeckIsFetching)(false));
    }
    if (action.type === "[SELECTED_PUBLIC_DECK] ADD_TO_MY_DECKS") {
        dispatch((0, selectedPublicDeck_actions_1.SetSelectedPublicDeckIsFetching)(true));
        publicDecksService
            .addToMyDecks(action.payload.publicDeckId)
            .then(function (response) {
            dispatch((0, selectedPublicDeck_actions_1.addToMyDecksSucceeded)(response));
        })
            .catch(function (e) {
            dispatch((0, selectedPublicDeck_actions_1.addToMyDecksFailed)(e.message));
        });
    }
    if (action.type === "[SELECTED_PUBLIC_DECK] ADD_TO_MY_DECKS_SUCCEEDED") {
        dispatch((0, selectedPublicDeck_actions_1.setSelectePublicDeckPrivateDeckId)(action.payload.privateDeckId));
    }
    if (action.type === "[SELECTED_PUBLIC_DECK] REQUEST APPROVE") {
        dispatch((0, selectedPublicDeck_actions_1.clearSelectedPublicDeckError)());
        var state = getState();
        publicDecksService
            .approveDeck(state.selectedPublicDeck.id)
            .then(function () {
            dispatch((0, selectedPublicDeck_actions_1.setSelectedPublicDeckApproved)(true));
        })
            .catch(function (error) {
            dispatch((0, selectedPublicDeck_actions_1.requestApprovalChangeFailed)(error.message));
        });
    }
    if (action.type === "[SELECTED_PUBLIC_DECK] REQUEST UNAPPROVE") {
        dispatch((0, selectedPublicDeck_actions_1.clearSelectedPublicDeckError)());
        var state = getState();
        publicDecksService
            .unapproveDeck(state.selectedPublicDeck.id)
            .then(function () {
            dispatch((0, selectedPublicDeck_actions_1.setSelectedPublicDeckApproved)(false));
        })
            .catch(function (error) {
            dispatch((0, selectedPublicDeck_actions_1.requestApprovalChangeFailed)(error.message));
        });
    }
    if (action.type === "[SELECTED_PUBLIC_DECK] REQUEST APPROVAL CHANGE FAILED") {
        dispatch((0, selectedPublicDeck_actions_1.setSelectedPublicDeckError)(action.payload.message));
    }
};
exports.selectedPublicDeckMiddleware = selectedPublicDeckMiddleware;
//# sourceMappingURL=selectedPublicDeck.middleware.js.map