import { SelectedPublicDeckActions } from "../actions/selectedPublicDeck.actions";
import { SelectedPublicDeckState } from "../reducers/selectedPublicDeckReducer";
import { ReducerMiddleware } from "./reducerMiddleware";
export declare const selectedPublicDeckMiddleware: ReducerMiddleware<SelectedPublicDeckState, SelectedPublicDeckActions>;
//# sourceMappingURL=selectedPublicDeck.middleware.d.ts.map