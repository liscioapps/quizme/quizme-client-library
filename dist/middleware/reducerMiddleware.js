"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createMiddleWare = void 0;
function createMiddleWare(reducerMiddleWare, selector) {
    return function (api) { return function (next) { return function (action) {
        var selectPartialState = function () { return selector(api.getState()); };
        reducerMiddleWare(selectPartialState, api.dispatch, next, action);
    }; }; };
}
exports.createMiddleWare = createMiddleWare;
//# sourceMappingURL=reducerMiddleware.js.map