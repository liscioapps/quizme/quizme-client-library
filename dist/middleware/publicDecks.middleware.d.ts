import { PublicDeckActions } from "../actions/publicDecks.actions";
import { PublicDeckListState } from "../reducers/publicDeckList.reducer";
import { ReducerMiddleware } from "./reducerMiddleware";
export declare const publicDecksMiddleware: ReducerMiddleware<PublicDeckListState, PublicDeckActions>;
//# sourceMappingURL=publicDecks.middleware.d.ts.map