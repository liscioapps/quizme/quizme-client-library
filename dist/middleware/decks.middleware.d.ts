import { DeckActions } from '../actions/decks.actions';
import { DeckListState } from '../reducers/deckList.reducer';
import { ReducerMiddleware } from './reducerMiddleware';
export declare const decksMiddleware: ReducerMiddleware<DeckListState, DeckActions>;
//# sourceMappingURL=decks.middleware.d.ts.map