"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.decksMiddleware = void 0;
var decks_actions_1 = require("../actions/decks.actions");
var services_1 = require("../services");
var decksMiddleware = function (getState, dispatch, next, action) {
    next(action);
    var decksService = services_1.DeckCRUDService.Instance;
    if (action.type === '[DECKLIST] INITIAL_FETCH_LIST') {
        var state = getState();
        if (state.isFetching) {
            // one request cycle at a time
            return;
        }
        dispatch((0, decks_actions_1.setDeckListHasInitialLoad)(false));
        dispatch((0, decks_actions_1.setDeckListFetching)(true));
        var _a = state.currentPaginationSettings, orderBy = _a.orderBy, orderDirection = _a.orderDirection, pageSize = _a.pageSize;
        var searchTerm = state.searchTerm;
        var otherFilters = {};
        if (searchTerm) {
            otherFilters['Title'] = "*".concat(searchTerm, "*");
        }
        decksService
            .getList(pageSize, 0, orderDirection === 'desc' ? "-".concat(orderBy) : "".concat(orderBy), [], otherFilters)
            .then(function (response) {
            dispatch((0, decks_actions_1.fetchDecksSucceded)(response.data, response.resultPageIndex, response.resultTotals, response.didReachEnd, 'initial')); // initial fetch success
        })
            .catch(function (e) {
            dispatch((0, decks_actions_1.fetchDecksFailed)(e.message));
        });
    }
    if (action.type === '[DECKLIST] FETCH_NEXT_LIST_PAGE') {
        var state = getState();
        if (state.isFetching) {
            // no duplicate requests
            return;
        }
        dispatch((0, decks_actions_1.setDeckListFetching)(true));
        var _b = state.currentPaginationSettings, orderBy = _b.orderBy, orderDirection = _b.orderDirection, pageIndex = _b.pageIndex, pageSize = _b.pageSize;
        var searchTerm = state.searchTerm;
        var otherFilters = {};
        if (searchTerm) {
            otherFilters['Title'] = "*".concat(searchTerm, "*");
        }
        decksService
            .getList(pageSize, pageIndex + 1, orderDirection === 'desc' ? "-".concat(orderBy) : "".concat(orderBy), [], otherFilters)
            .then(function (response) {
            dispatch((0, decks_actions_1.fetchDecksSucceded)(response.data, response.resultPageIndex, response.resultTotals, response.didReachEnd, 'nextPage'));
        })
            .catch(function () {
            dispatch((0, decks_actions_1.fetchDecksFailed)('Faled to fetch decks'));
        });
    }
    if (action.type === '[DECKLIST] FETCH_DECKS_FAILURE') {
        dispatch((0, decks_actions_1.setDeckListFetching)(false));
        dispatch((0, decks_actions_1.setNotification)(action.payload.errorMsg));
    }
    if (action.type === '[DECKLIST] FETCH_DECKS_SUCCEDED') {
        dispatch((0, decks_actions_1.setDeckListFetching)(false));
        dispatch((0, decks_actions_1.setPagination)(action.meta.newPage, action.meta.totalRecords, action.meta.reachedEnd));
        if (action.meta.context === 'initial') {
            dispatch((0, decks_actions_1.setDecks)(action.payload));
            dispatch((0, decks_actions_1.setDeckListHasInitialLoad)(true));
        }
        else if (action.meta.context === 'nextPage') {
            dispatch((0, decks_actions_1.appendDecks)(action.payload));
        }
    }
    if (action.type === '[DECKLIST] SORT_PROPERTIES_CHANGED' ||
        action.type === '[DECKLIST] SEARCH_TERM_CHANGED') {
        dispatch((0, decks_actions_1.fetchInitialDeckList)());
    }
};
exports.decksMiddleware = decksMiddleware;
//# sourceMappingURL=decks.middleware.js.map