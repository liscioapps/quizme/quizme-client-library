"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.publicDecksMiddleware = void 0;
var publicDecks_actions_1 = require("../actions/publicDecks.actions");
var services_1 = require("../services");
var publicDecksMiddleware = function (getState, dispatch, next, action) {
    next(action);
    var publicDecksService = services_1.PublicDeckCRUDService.Instance;
    if (action.type === "[PUBLIC_DECKLIST] INITIAL_FETCH_LIST") {
        var state = getState();
        if (state.isFetching) {
            // one request cycle at a time
            return;
        }
        dispatch((0, publicDecks_actions_1.setPublicDeckListHasInitialLoad)(false));
        dispatch((0, publicDecks_actions_1.setPublicDeckListFetching)(true));
        var _a = state.currentPaginationSettings, orderBy = _a.orderBy, orderDirection = _a.orderDirection, pageSize = _a.pageSize;
        var searchTerm = state.searchTerm;
        var otherFilters = {};
        if (searchTerm) {
            otherFilters["Title"] = "*".concat(searchTerm, "*");
        }
        publicDecksService
            .getList(10, 0, orderDirection === "desc" ? "-".concat(orderBy) : "".concat(orderBy), [], otherFilters)
            .then(function (response) {
            dispatch((0, publicDecks_actions_1.fetchPublicDecksSucceded)(response.data, response.resultPageIndex, response.resultTotals, response.didReachEnd, "initial")); // initial fetch success
        })
            .catch(function () {
            dispatch((0, publicDecks_actions_1.fetchPublicDecksFailed)("Faled to fetch decks"));
        });
    }
    if (action.type === "[PUBLIC_DECKLIST] FETCH_NEXT_LIST_PAGE") {
        var state = getState();
        if (state.isFetching) {
            // no duplicate requests
            return;
        }
        dispatch((0, publicDecks_actions_1.setPublicDeckListFetching)(true));
        var _b = state.currentPaginationSettings, orderBy = _b.orderBy, orderDirection = _b.orderDirection, pageIndex = _b.pageIndex, pageSize = _b.pageSize;
        var searchTerm = state.searchTerm;
        var otherFilters = {};
        if (searchTerm) {
            otherFilters["Title"] = "*".concat(searchTerm, "*");
        }
        publicDecksService
            .getList(pageSize, pageIndex + 1, orderDirection === "desc" ? "-".concat(orderBy) : "".concat(orderBy), [], otherFilters)
            .then(function (response) {
            dispatch((0, publicDecks_actions_1.fetchPublicDecksSucceded)(response.data, response.resultPageIndex, response.resultTotals, response.didReachEnd, "nextPage"));
        })
            .catch(function () {
            dispatch((0, publicDecks_actions_1.fetchPublicDecksFailed)("Faled to fetch decks"));
        });
    }
    if (action.type === "[PUBLIC_DECKLIST] FETCH_DECKS_FAILURE") {
        dispatch((0, publicDecks_actions_1.setPublicDeckListFetching)(false));
        dispatch((0, publicDecks_actions_1.setPublicDeckListNotification)(action.payload.errorMsg));
    }
    if (action.type === "[PUBLIC_DECKLIST] FETCH_DECKS_SUCCEDED") {
        dispatch((0, publicDecks_actions_1.setPublicDeckListFetching)(false));
        dispatch((0, publicDecks_actions_1.setPublicDeckPagination)(action.meta.newPage, action.meta.totalRecords, action.meta.reachedEnd));
        if (action.meta.context === "initial") {
            dispatch((0, publicDecks_actions_1.setPublicDecks)(action.payload));
            dispatch((0, publicDecks_actions_1.setPublicDeckListHasInitialLoad)(true));
        }
        else if (action.meta.context === "nextPage") {
            dispatch((0, publicDecks_actions_1.appendPublicDecks)(action.payload));
        }
    }
    if (action.type === "[PUBLIC_DECKLIST] SEARCH_TERM_CHANGED") {
        dispatch((0, publicDecks_actions_1.fetchInitialPublicDeckList)());
    }
};
exports.publicDecksMiddleware = publicDecksMiddleware;
//# sourceMappingURL=publicDecks.middleware.js.map