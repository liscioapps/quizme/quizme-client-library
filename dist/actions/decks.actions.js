"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.removeNotification = exports.setNotification = exports.setDeckListHasInitialLoad = exports.setDeckListFetching = exports.setPagination = exports.appendDecks = exports.setDecks = exports.setOrderByDescending = exports.setOrderByAscending = exports.setPageIndex = exports.setPageSize = exports.fetchNextDeckListPage = exports.fetchInitialDeckList = exports.searchTermChanged = exports.sortPropertiesChanged = exports.fetchDecksFailed = exports.fetchDecksSucceded = void 0;
var toolkit_1 = require("@reduxjs/toolkit");
var DECKLIST = "[DECKLIST]";
var fetchDecksSucceded = function (decks, newPage, totalRecords, reachedEnd, context) {
    return {
        type: "[DECKLIST] FETCH_DECKS_SUCCEDED",
        payload: decks,
        meta: {
            context: context,
            newPage: newPage,
            totalRecords: totalRecords,
            reachedEnd: reachedEnd,
        },
    };
};
exports.fetchDecksSucceded = fetchDecksSucceded;
var fetchDecksFailed = function (errorMessage) {
    return {
        type: "[DECKLIST] FETCH_DECKS_FAILURE",
        payload: {
            errorMsg: errorMessage,
        },
    };
};
exports.fetchDecksFailed = fetchDecksFailed;
var sortPropertiesChanged = function (field, direction) {
    return {
        type: "[DECKLIST] SORT_PROPERTIES_CHANGED",
        payload: {
            field: field,
            direction: direction,
        },
    };
};
exports.sortPropertiesChanged = sortPropertiesChanged;
var searchTermChanged = function (searchTerm) {
    return {
        type: "[DECKLIST] SEARCH_TERM_CHANGED",
        payload: searchTerm,
    };
};
exports.searchTermChanged = searchTermChanged;
var fetchInitialDeckList = function () { return ({
    type: "[DECKLIST] INITIAL_FETCH_LIST",
}); };
exports.fetchInitialDeckList = fetchInitialDeckList;
var fetchNextDeckListPage = function () { return ({
    type: "[DECKLIST] FETCH_NEXT_LIST_PAGE",
}); };
exports.fetchNextDeckListPage = fetchNextDeckListPage;
var setPageSize = function (pageSize) {
    return {
        type: "[DECKLIST] SET_PAGE_SIZE",
        payload: pageSize,
    };
};
exports.setPageSize = setPageSize;
var setPageIndex = function () {
    return {
        type: "[DECKLIST] INCREMENT_PAGE_INDEX",
    };
};
exports.setPageIndex = setPageIndex;
var setOrderByAscending = function (field) {
    return {
        type: "[DECKLIST] SET_ORDER_BY_ASCENDING",
        payload: {
            field: field,
        },
    };
};
exports.setOrderByAscending = setOrderByAscending;
var setOrderByDescending = function (field) {
    return {
        type: "[DECKLIST] SET_ORDER_BY_DESCENDING",
        payload: {
            field: field,
        },
    };
};
exports.setOrderByDescending = setOrderByDescending;
var setDecks = function (decks) { return ({
    type: "[DECKLIST] SET_DECKS",
    payload: decks,
}); };
exports.setDecks = setDecks;
var appendDecks = function (decks) { return ({
    type: "[DECKLIST] APPEND_DECKS",
    payload: decks,
}); };
exports.appendDecks = appendDecks;
var setPagination = function (currentPage, totalRecords, didReachEnd) {
    return {
        type: "[DECKLIST] SET_PAGINATION",
        payload: {
            currentPage: currentPage,
            totalRecords: totalRecords,
            didReachEnd: didReachEnd,
        },
    };
};
exports.setPagination = setPagination;
var setDeckListFetching = function (loading) {
    return {
        type: "[DECKLIST] SET_FETCHING",
        payload: loading,
    };
};
exports.setDeckListFetching = setDeckListFetching;
var setDeckListHasInitialLoad = function (hasInitialLoad) {
    return {
        type: "[DECKLIST] SET_HAS_INITIAL_LOAD",
        payload: hasInitialLoad,
    };
};
exports.setDeckListHasInitialLoad = setDeckListHasInitialLoad;
var setNotification = function (message) {
    return {
        type: "[DECKLIST] SET_NOTIFICATION",
        payload: {
            message: message,
            id: (0, toolkit_1.nanoid)(),
        },
    };
};
exports.setNotification = setNotification;
var removeNotification = function (notificationId) {
    return {
        type: "[DECKLIST] REMOVE_NOTIFICATION",
        payload: {
            notificationId: notificationId,
        },
    };
};
exports.removeNotification = removeNotification;
//# sourceMappingURL=decks.actions.js.map