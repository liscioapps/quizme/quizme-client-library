import { PublicDeckModel } from "../models/PublicDeckModel";
declare const PUBLIC_DECKS = "[SELECTED_PUBLIC_DECK]";
interface FetchPublicDeckSucceded {
    type: `${typeof PUBLIC_DECKS} FETCH_SUCCEDED`;
    payload: PublicDeckModel;
}
export declare const fetchSelectedPublicDeckSucceeded: (deck: PublicDeckModel) => FetchPublicDeckSucceded;
interface FetchPublicDeckFailed {
    type: `${typeof PUBLIC_DECKS} FETCH_DECK_FAILURE`;
    payload: {
        errorMsg: string;
    };
}
export declare const fetchSelectedPublicDeckFailed: (errorMessage: string) => FetchPublicDeckFailed;
interface AddToMyDecksFailed {
    type: `${typeof PUBLIC_DECKS} ADD_TO_MY_DECKS_FAILED`;
    payload: {
        errorMsg: string;
    };
}
export declare const addToMyDecksFailed: (errorMessage: string) => AddToMyDecksFailed;
interface AddToMyDecksSucceeded {
    type: `${typeof PUBLIC_DECKS} ADD_TO_MY_DECKS_SUCCEEDED`;
    payload: {
        privateDeckId: number;
    };
}
export declare const addToMyDecksSucceeded: (privateDeckId: number) => AddToMyDecksSucceeded;
interface FetchPublicDeck {
    type: `${typeof PUBLIC_DECKS} FETCH_PUBLIC_DECK`;
    payload: {
        publicDeckId: number;
    };
}
export declare const fetchSelectedPublicDeck: (publicDeckId: number) => FetchPublicDeck;
interface AddPublicDeckToMyDecks {
    type: `${typeof PUBLIC_DECKS} ADD_TO_MY_DECKS`;
    payload: {
        publicDeckId: number;
    };
}
export declare const addPublicDeckToMyDecks: (publicDeckId: number) => AddPublicDeckToMyDecks;
interface SetSelectedPublicDeck {
    type: `${typeof PUBLIC_DECKS} SET_DECK`;
    payload: {
        deck: PublicDeckModel;
    };
}
export declare const setSelectedPublicDeck: (deck: PublicDeckModel) => SetSelectedPublicDeck;
interface SetSelectePublicDeckPrivateDeckId {
    type: `${typeof PUBLIC_DECKS} SET_PRIVATE_DECK_ID`;
    payload: {
        privateDeckId: number;
    };
}
export declare const setSelectePublicDeckPrivateDeckId: (privateDeckId: number) => SetSelectePublicDeckPrivateDeckId;
interface SetSelectedPublicDeckIsFetching {
    type: `${typeof PUBLIC_DECKS} SET_IS_FETCHING`;
    payload: {
        isFetching: boolean;
    };
}
export declare const SetSelectedPublicDeckIsFetching: (isFetching: boolean) => SetSelectedPublicDeckIsFetching;
interface SetSelectedPublicDeckError {
    type: `${typeof PUBLIC_DECKS} SET_ERROR`;
    payload: {
        errorMessage: string;
    };
}
export declare const setSelectedPublicDeckError: (errorMessage: string) => SetSelectedPublicDeckError;
interface ClearSelectedPublicDeckError {
    type: `${typeof PUBLIC_DECKS} CLEAR_ERROR`;
}
export declare const clearSelectedPublicDeckError: () => ClearSelectedPublicDeckError;
interface RequestApproveSelectedPublicDeck {
    type: `${typeof PUBLIC_DECKS} REQUEST APPROVE`;
}
export declare const requestApproveSelectedPublicDeck: () => RequestApproveSelectedPublicDeck;
interface RequestUnapproveSelectedPublicDeck {
    type: `${typeof PUBLIC_DECKS} REQUEST UNAPPROVE`;
}
export declare const requestUnapproveSelectedPublicDeck: () => RequestUnapproveSelectedPublicDeck;
interface SetSelectedPublicDeckApproved {
    type: `${typeof PUBLIC_DECKS} SET APPROVED`;
    payload: {
        approved: boolean;
    };
}
export declare const setSelectedPublicDeckApproved: (approved: boolean) => SetSelectedPublicDeckApproved;
interface RequestApprovalChangeFailed {
    type: `${typeof PUBLIC_DECKS} REQUEST APPROVAL CHANGE FAILED`;
    payload: {
        message: string;
    };
}
export declare const requestApprovalChangeFailed: (message?: string) => RequestApprovalChangeFailed;
export declare type SelectedPublicDeckActions = FetchPublicDeckSucceded | FetchPublicDeckFailed | FetchPublicDeck | AddPublicDeckToMyDecks | SetSelectedPublicDeck | SetSelectedPublicDeckIsFetching | SetSelectedPublicDeckError | ClearSelectedPublicDeckError | SetSelectePublicDeckPrivateDeckId | AddToMyDecksFailed | AddToMyDecksSucceeded | RequestApproveSelectedPublicDeck | RequestUnapproveSelectedPublicDeck | SetSelectedPublicDeckApproved | RequestApprovalChangeFailed;
export declare type SelectedPublicDeckActionsTypes = SelectedPublicDeckActions["type"];
export {};
//# sourceMappingURL=selectedPublicDeck.actions.d.ts.map