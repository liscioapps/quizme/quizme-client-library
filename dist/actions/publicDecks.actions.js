"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.removePubilcDeckListNotification = exports.setPublicDeckListNotification = exports.setPublicDeckListHasInitialLoad = exports.setPublicDeckListFetching = exports.setPublicDeckPagination = exports.appendPublicDecks = exports.setPublicDecks = exports.setPublicPageIndex = exports.setPublicPageSize = exports.fetchNextPublicDeckListPage = exports.fetchInitialPublicDeckList = exports.publicSearchTermChanged = exports.fetchPublicDecksFailed = exports.fetchPublicDecksSucceded = void 0;
var toolkit_1 = require("@reduxjs/toolkit");
var PUBLIC_DECKS = '[PUBLIC_DECKLIST]';
var fetchPublicDecksSucceded = function (decks, newPage, totalRecords, reachedEnd, context) {
    return {
        type: '[PUBLIC_DECKLIST] FETCH_DECKS_SUCCEDED',
        payload: decks,
        meta: {
            context: context,
            newPage: newPage,
            totalRecords: totalRecords,
            reachedEnd: reachedEnd,
        },
    };
};
exports.fetchPublicDecksSucceded = fetchPublicDecksSucceded;
var fetchPublicDecksFailed = function (errorMessage) {
    return {
        type: '[PUBLIC_DECKLIST] FETCH_DECKS_FAILURE',
        payload: {
            errorMsg: errorMessage,
        },
    };
};
exports.fetchPublicDecksFailed = fetchPublicDecksFailed;
var publicSearchTermChanged = function (searchTerm) {
    return {
        type: '[PUBLIC_DECKLIST] SEARCH_TERM_CHANGED',
        payload: searchTerm,
    };
};
exports.publicSearchTermChanged = publicSearchTermChanged;
var fetchInitialPublicDeckList = function () { return ({
    type: '[PUBLIC_DECKLIST] INITIAL_FETCH_LIST',
}); };
exports.fetchInitialPublicDeckList = fetchInitialPublicDeckList;
var fetchNextPublicDeckListPage = function () { return ({
    type: '[PUBLIC_DECKLIST] FETCH_NEXT_LIST_PAGE',
}); };
exports.fetchNextPublicDeckListPage = fetchNextPublicDeckListPage;
var setPublicPageSize = function (pageSize) {
    return {
        type: '[PUBLIC_DECKLIST] SET_PAGE_SIZE',
        payload: pageSize,
    };
};
exports.setPublicPageSize = setPublicPageSize;
var setPublicPageIndex = function () {
    return {
        type: '[PUBLIC_DECKLIST] INCREMENT_PAGE_INDEX',
    };
};
exports.setPublicPageIndex = setPublicPageIndex;
var setPublicDecks = function (decks) { return ({
    type: "[PUBLIC_DECKLIST] SET_DECKS",
    payload: decks,
}); };
exports.setPublicDecks = setPublicDecks;
var appendPublicDecks = function (decks) { return ({
    type: '[PUBLIC_DECKLIST] APPEND_DECKS',
    payload: decks,
}); };
exports.appendPublicDecks = appendPublicDecks;
var setPublicDeckPagination = function (currentPage, totalRecords, didReachEnd) {
    return {
        type: '[PUBLIC_DECKLIST] SET_PAGINATION',
        payload: {
            currentPage: currentPage,
            totalRecords: totalRecords,
            didReachEnd: didReachEnd,
        },
    };
};
exports.setPublicDeckPagination = setPublicDeckPagination;
var setPublicDeckListFetching = function (loading) {
    return {
        type: '[PUBLIC_DECKLIST] SET_FETCHING',
        payload: loading,
    };
};
exports.setPublicDeckListFetching = setPublicDeckListFetching;
var setPublicDeckListHasInitialLoad = function (hasInitialLoad) {
    return {
        type: '[PUBLIC_DECKLIST] SET_HAS_INITIAL_LOAD',
        payload: hasInitialLoad,
    };
};
exports.setPublicDeckListHasInitialLoad = setPublicDeckListHasInitialLoad;
var setPublicDeckListNotification = function (message) {
    return {
        type: '[PUBLIC_DECKLIST] SET_NOTIFICATION',
        payload: {
            message: message,
            id: (0, toolkit_1.nanoid)(),
        },
    };
};
exports.setPublicDeckListNotification = setPublicDeckListNotification;
var removePubilcDeckListNotification = function (notificationId) {
    return {
        type: '[PUBLIC_DECKLIST] REMOVE_NOTIFICATION',
        payload: {
            notificationId: notificationId,
        },
    };
};
exports.removePubilcDeckListNotification = removePubilcDeckListNotification;
//# sourceMappingURL=publicDecks.actions.js.map