"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.requestApprovalChangeFailed = exports.setSelectedPublicDeckApproved = exports.requestUnapproveSelectedPublicDeck = exports.requestApproveSelectedPublicDeck = exports.clearSelectedPublicDeckError = exports.setSelectedPublicDeckError = exports.SetSelectedPublicDeckIsFetching = exports.setSelectePublicDeckPrivateDeckId = exports.setSelectedPublicDeck = exports.addPublicDeckToMyDecks = exports.fetchSelectedPublicDeck = exports.addToMyDecksSucceeded = exports.addToMyDecksFailed = exports.fetchSelectedPublicDeckFailed = exports.fetchSelectedPublicDeckSucceeded = void 0;
var PUBLIC_DECKS = "[SELECTED_PUBLIC_DECK]";
var fetchSelectedPublicDeckSucceeded = function (deck) {
    return {
        type: "[SELECTED_PUBLIC_DECK] FETCH_SUCCEDED",
        payload: deck,
    };
};
exports.fetchSelectedPublicDeckSucceeded = fetchSelectedPublicDeckSucceeded;
var fetchSelectedPublicDeckFailed = function (errorMessage) {
    return {
        type: "[SELECTED_PUBLIC_DECK] FETCH_DECK_FAILURE",
        payload: {
            errorMsg: errorMessage,
        },
    };
};
exports.fetchSelectedPublicDeckFailed = fetchSelectedPublicDeckFailed;
var addToMyDecksFailed = function (errorMessage) {
    return {
        type: "[SELECTED_PUBLIC_DECK] ADD_TO_MY_DECKS_FAILED",
        payload: {
            errorMsg: errorMessage,
        },
    };
};
exports.addToMyDecksFailed = addToMyDecksFailed;
var addToMyDecksSucceeded = function (privateDeckId) {
    return {
        type: "[SELECTED_PUBLIC_DECK] ADD_TO_MY_DECKS_SUCCEEDED",
        payload: {
            privateDeckId: privateDeckId,
        },
    };
};
exports.addToMyDecksSucceeded = addToMyDecksSucceeded;
var fetchSelectedPublicDeck = function (publicDeckId) { return ({
    type: "[SELECTED_PUBLIC_DECK] FETCH_PUBLIC_DECK",
    payload: {
        publicDeckId: publicDeckId,
    },
}); };
exports.fetchSelectedPublicDeck = fetchSelectedPublicDeck;
var addPublicDeckToMyDecks = function (publicDeckId) { return ({
    type: "[SELECTED_PUBLIC_DECK] ADD_TO_MY_DECKS",
    payload: {
        publicDeckId: publicDeckId,
    },
}); };
exports.addPublicDeckToMyDecks = addPublicDeckToMyDecks;
var setSelectedPublicDeck = function (deck) {
    return {
        type: "[SELECTED_PUBLIC_DECK] SET_DECK",
        payload: {
            deck: deck,
        },
    };
};
exports.setSelectedPublicDeck = setSelectedPublicDeck;
var setSelectePublicDeckPrivateDeckId = function (privateDeckId) {
    return {
        type: "[SELECTED_PUBLIC_DECK] SET_PRIVATE_DECK_ID",
        payload: {
            privateDeckId: privateDeckId,
        },
    };
};
exports.setSelectePublicDeckPrivateDeckId = setSelectePublicDeckPrivateDeckId;
var SetSelectedPublicDeckIsFetching = function (isFetching) {
    return {
        type: "[SELECTED_PUBLIC_DECK] SET_IS_FETCHING",
        payload: {
            isFetching: isFetching,
        },
    };
};
exports.SetSelectedPublicDeckIsFetching = SetSelectedPublicDeckIsFetching;
var setSelectedPublicDeckError = function (errorMessage) {
    return {
        type: "[SELECTED_PUBLIC_DECK] SET_ERROR",
        payload: {
            errorMessage: errorMessage,
        },
    };
};
exports.setSelectedPublicDeckError = setSelectedPublicDeckError;
var clearSelectedPublicDeckError = function () {
    return {
        type: "[SELECTED_PUBLIC_DECK] CLEAR_ERROR",
    };
};
exports.clearSelectedPublicDeckError = clearSelectedPublicDeckError;
var requestApproveSelectedPublicDeck = function () {
    return {
        type: "[SELECTED_PUBLIC_DECK] REQUEST APPROVE",
    };
};
exports.requestApproveSelectedPublicDeck = requestApproveSelectedPublicDeck;
var requestUnapproveSelectedPublicDeck = function () {
    return {
        type: "[SELECTED_PUBLIC_DECK] REQUEST UNAPPROVE",
    };
};
exports.requestUnapproveSelectedPublicDeck = requestUnapproveSelectedPublicDeck;
var setSelectedPublicDeckApproved = function (approved) {
    return {
        type: "[SELECTED_PUBLIC_DECK] SET APPROVED",
        payload: {
            approved: approved,
        },
    };
};
exports.setSelectedPublicDeckApproved = setSelectedPublicDeckApproved;
var requestApprovalChangeFailed = function (message) {
    if (message === void 0) { message = "Error updating approval status"; }
    return {
        type: "[SELECTED_PUBLIC_DECK] REQUEST APPROVAL CHANGE FAILED",
        payload: {
            message: message,
        },
    };
};
exports.requestApprovalChangeFailed = requestApprovalChangeFailed;
//# sourceMappingURL=selectedPublicDeck.actions.js.map