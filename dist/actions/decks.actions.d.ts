import { DeckModel, DeckSummaryModel } from "../models";
declare const DECKLIST = "[DECKLIST]";
interface FetchDecksSucceded {
    type: `${typeof DECKLIST} FETCH_DECKS_SUCCEDED`;
    payload: DeckSummaryModel[];
    meta: {
        context: "initial" | "nextPage";
        newPage: number;
        totalRecords: number;
        reachedEnd: boolean;
    };
}
export declare const fetchDecksSucceded: (decks: DeckSummaryModel[], newPage: number, totalRecords: number, reachedEnd: boolean, context: "initial" | "nextPage") => FetchDecksSucceded;
interface FetchDecksFailed {
    type: `${typeof DECKLIST} FETCH_DECKS_FAILURE`;
    payload: {
        errorMsg: string;
    };
}
export declare const fetchDecksFailed: (errorMessage: string) => FetchDecksFailed;
interface SortPropertiesChanged {
    type: `${typeof DECKLIST} SORT_PROPERTIES_CHANGED`;
    payload: {
        field: string;
        direction: "asc" | "desc";
    };
}
export declare const sortPropertiesChanged: (field: string, direction: "asc" | "desc") => SortPropertiesChanged;
interface SearchTermChanged {
    type: `${typeof DECKLIST} SEARCH_TERM_CHANGED`;
    payload: string;
}
export declare const searchTermChanged: (searchTerm: string) => SearchTermChanged;
interface FetchInitialDeckListAction {
    type: `${typeof DECKLIST} INITIAL_FETCH_LIST`;
}
export declare const fetchInitialDeckList: () => FetchInitialDeckListAction;
interface FetchNextDeckListPage {
    type: `${typeof DECKLIST} FETCH_NEXT_LIST_PAGE`;
}
export declare const fetchNextDeckListPage: () => FetchNextDeckListPage;
interface SetPageSize {
    type: `${typeof DECKLIST} SET_PAGE_SIZE`;
    payload: number;
}
export declare const setPageSize: (pageSize: number) => SetPageSize;
interface IncrementPageIndex {
    type: `${typeof DECKLIST} INCREMENT_PAGE_INDEX`;
}
export declare const setPageIndex: () => IncrementPageIndex;
interface SetOrderByAscending {
    type: `${typeof DECKLIST} SET_ORDER_BY_ASCENDING`;
    payload: {
        field: string;
    };
}
export declare const setOrderByAscending: (field: string) => SetOrderByAscending;
interface SetOrderByDescending {
    type: `${typeof DECKLIST} SET_ORDER_BY_DESCENDING`;
    payload: {
        field: string;
    };
}
export declare const setOrderByDescending: (field: string) => SetOrderByDescending;
interface SetDecks {
    type: `${typeof DECKLIST} SET_DECKS`;
    payload: DeckSummaryModel[];
}
export declare const setDecks: (decks: DeckModel[]) => SetDecks;
interface AppendDecks {
    type: `${typeof DECKLIST} APPEND_DECKS`;
    payload: DeckSummaryModel[];
}
export declare const appendDecks: (decks: DeckModel[]) => AppendDecks;
interface SetPagination {
    type: `${typeof DECKLIST} SET_PAGINATION`;
    payload: {
        currentPage: number;
        totalRecords: number;
        didReachEnd: boolean;
    };
}
export declare const setPagination: (currentPage: number, totalRecords: number, didReachEnd: boolean) => SetPagination;
interface SetDeckListFetching {
    type: `${typeof DECKLIST} SET_FETCHING`;
    payload: boolean;
}
export declare const setDeckListFetching: (loading: boolean) => SetDeckListFetching;
interface SetHasInitialLoad {
    type: `${typeof DECKLIST} SET_HAS_INITIAL_LOAD`;
    payload: boolean;
}
export declare const setDeckListHasInitialLoad: (hasInitialLoad: boolean) => SetHasInitialLoad;
interface SetNotification {
    type: `${typeof DECKLIST} SET_NOTIFICATION`;
    payload: {
        message: string;
        id: string;
    };
}
export declare const setNotification: (message: string) => SetNotification;
interface RemoveNotification {
    type: `${typeof DECKLIST} REMOVE_NOTIFICATION`;
    payload: {
        notificationId: string;
    };
}
export declare const removeNotification: (notificationId: string) => RemoveNotification;
export declare type DeckActions = FetchInitialDeckListAction | FetchNextDeckListPage | SetDecks | AppendDecks | FetchDecksSucceded | FetchDecksFailed | SetPageSize | IncrementPageIndex | SetOrderByAscending | SetOrderByDescending | SetPagination | SortPropertiesChanged | SearchTermChanged | SetDeckListFetching | SetHasInitialLoad;
export declare type DeckActionTypes = DeckActions["type"];
export {};
//# sourceMappingURL=decks.actions.d.ts.map