import { PublicDeckSummaryModel } from '../models/PublicDeckSummaryModel';
declare const PUBLIC_DECKS = "[PUBLIC_DECKLIST]";
interface FetchPublicDecksSucceded {
    type: `${typeof PUBLIC_DECKS} FETCH_DECKS_SUCCEDED`;
    payload: PublicDeckSummaryModel[];
    meta: {
        context: 'initial' | 'nextPage';
        newPage: number;
        totalRecords: number;
        reachedEnd: boolean;
    };
}
export declare const fetchPublicDecksSucceded: (decks: PublicDeckSummaryModel[], newPage: number, totalRecords: number, reachedEnd: boolean, context: 'initial' | 'nextPage') => FetchPublicDecksSucceded;
interface FetchPublicDecksFailed {
    type: `${typeof PUBLIC_DECKS} FETCH_DECKS_FAILURE`;
    payload: {
        errorMsg: string;
    };
}
export declare const fetchPublicDecksFailed: (errorMessage: string) => FetchPublicDecksFailed;
interface PublicSearchTermChanged {
    type: `${typeof PUBLIC_DECKS} SEARCH_TERM_CHANGED`;
    payload: string;
}
export declare const publicSearchTermChanged: (searchTerm: string) => PublicSearchTermChanged;
interface FetchInitialPublicDeckListAction {
    type: `${typeof PUBLIC_DECKS} INITIAL_FETCH_LIST`;
}
export declare const fetchInitialPublicDeckList: () => FetchInitialPublicDeckListAction;
interface FetchNextPublicDeckListPage {
    type: `${typeof PUBLIC_DECKS} FETCH_NEXT_LIST_PAGE`;
}
export declare const fetchNextPublicDeckListPage: () => FetchNextPublicDeckListPage;
interface SetPublicPageSize {
    type: `${typeof PUBLIC_DECKS} SET_PAGE_SIZE`;
    payload: number;
}
export declare const setPublicPageSize: (pageSize: number) => SetPublicPageSize;
interface IncrementPublicPageIndex {
    type: `${typeof PUBLIC_DECKS} INCREMENT_PAGE_INDEX`;
}
export declare const setPublicPageIndex: () => IncrementPublicPageIndex;
interface SetPublicDecks {
    type: `${typeof PUBLIC_DECKS} SET_DECKS`;
    payload: PublicDeckSummaryModel[];
}
export declare const setPublicDecks: (decks: PublicDeckSummaryModel[]) => SetPublicDecks;
interface AppendPublicDecks {
    type: `${typeof PUBLIC_DECKS} APPEND_DECKS`;
    payload: PublicDeckSummaryModel[];
}
export declare const appendPublicDecks: (decks: PublicDeckSummaryModel[]) => AppendPublicDecks;
interface SetPublicDeckPagination {
    type: `${typeof PUBLIC_DECKS} SET_PAGINATION`;
    payload: {
        currentPage: number;
        totalRecords: number;
        didReachEnd: boolean;
    };
}
export declare const setPublicDeckPagination: (currentPage: number, totalRecords: number, didReachEnd: boolean) => SetPublicDeckPagination;
interface SetPublicDeckListFetching {
    type: `${typeof PUBLIC_DECKS} SET_FETCHING`;
    payload: boolean;
}
export declare const setPublicDeckListFetching: (loading: boolean) => SetPublicDeckListFetching;
interface SetPublicDeckListHasInitialLoad {
    type: `${typeof PUBLIC_DECKS} SET_HAS_INITIAL_LOAD`;
    payload: boolean;
}
export declare const setPublicDeckListHasInitialLoad: (hasInitialLoad: boolean) => SetPublicDeckListHasInitialLoad;
interface SetPublicDeckListNotification {
    type: `${typeof PUBLIC_DECKS} SET_NOTIFICATION`;
    payload: {
        message: string;
        id: string;
    };
}
export declare const setPublicDeckListNotification: (message: string) => SetPublicDeckListNotification;
interface RemovePublicDeckListNotification {
    type: `${typeof PUBLIC_DECKS} REMOVE_NOTIFICATION`;
    payload: {
        notificationId: string;
    };
}
export declare const removePubilcDeckListNotification: (notificationId: string) => RemovePublicDeckListNotification;
export declare type PublicDeckActions = FetchInitialPublicDeckListAction | FetchNextPublicDeckListPage | SetPublicDecks | AppendPublicDecks | FetchPublicDecksFailed | FetchPublicDecksSucceded | SetPublicPageSize | IncrementPublicPageIndex | SetPublicDeckPagination | PublicSearchTermChanged | SetPublicDeckListFetching | SetPublicDeckListHasInitialLoad | PublicSearchTermChanged;
export declare type PublicDeckActionTypes = PublicDeckActions['type'];
export {};
//# sourceMappingURL=publicDecks.actions.d.ts.map