import { AnyAction, Dispatch, Middleware } from 'redux';

// defines a function that is called by the stores middleware, but that is targets a part of the state and doesn't know about what the entire state looks like
export interface ReducerMiddleware<TState, TAction extends AnyAction> {
  (
    getPartialState: () => TState,
    next: Dispatch<AnyAction>,
    dispatch: Dispatch<AnyAction>,
    action: TAction
  ): void;
}

export function createMiddleWare<TState, TPartialState>(
  reducerMiddleWare: ReducerMiddleware<TPartialState, any>,
  selector: (state: TState) => TPartialState
): Middleware<{}, TState> {
  return (api) => (next) => (action) => {
    const selectPartialState = () => selector(api.getState());
    reducerMiddleWare(selectPartialState, api.dispatch, next, action);
  };
}
