import { AnyAction, Dispatch, Middleware } from 'redux';
import {
  appendDecks,
  DeckActions,
  fetchDecksFailed,
  fetchDecksSucceded,
  fetchInitialDeckList,
  setDeckListFetching,
  setDeckListHasInitialLoad,
  setDecks,
  setNotification,
  setPagination,
} from '../actions/decks.actions';
import { Index } from '../models/IndexHelper';
import { DeckListState } from '../reducers/deckList.reducer';
import { DeckCRUDService } from '../services';
import { ReducerMiddleware } from './reducerMiddleware';

export const decksMiddleware: ReducerMiddleware<DeckListState, DeckActions> = (
  getState,
  dispatch,
  next,
  action
) => {
  next(action);
  const decksService = DeckCRUDService.Instance;

  if (action.type === '[DECKLIST] INITIAL_FETCH_LIST') {
    const state = getState();
    if (state.isFetching) {
      // one request cycle at a time
      return;
    }

    dispatch(setDeckListHasInitialLoad(false));
    dispatch(setDeckListFetching(true));
    const { orderBy, orderDirection, pageSize } = state.currentPaginationSettings;
    const { searchTerm } = state;
    const otherFilters: Index<string> = {};

    if (searchTerm) {
      otherFilters['Title'] = `*${searchTerm}*`;
    }

    decksService
      .getList(
        pageSize,
        0,
        orderDirection === 'desc' ? `-${orderBy}` : `${orderBy}`,
        [],
        otherFilters
      )
      .then((response) => {
        dispatch(
          fetchDecksSucceded(
            response.data,
            response.resultPageIndex,
            response.resultTotals,
            response.didReachEnd,
            'initial'
          )
        ); // initial fetch success
      })
      .catch((e) => {
        dispatch(fetchDecksFailed(e.message));
      });
  }

  if (action.type === '[DECKLIST] FETCH_NEXT_LIST_PAGE') {
    const state = getState();
    if (state.isFetching) {
      // no duplicate requests
      return;
    }
    dispatch(setDeckListFetching(true));

    const { orderBy, orderDirection, pageIndex, pageSize } = state.currentPaginationSettings;
    const { searchTerm } = state;

    const otherFilters: Index<string> = {};
    if (searchTerm) {
      otherFilters['Title'] = `*${searchTerm}*`;
    }
    decksService
      .getList(
        pageSize,
        pageIndex + 1,
        orderDirection === 'desc' ? `-${orderBy}` : `${orderBy}`,
        [],
        otherFilters
      )
      .then((response) => {
        dispatch(
          fetchDecksSucceded(
            response.data,
            response.resultPageIndex,
            response.resultTotals,
            response.didReachEnd,
            'nextPage'
          )
        );
      })
      .catch(() => {
        dispatch(fetchDecksFailed('Faled to fetch decks'));
      });
  }

  if (action.type === '[DECKLIST] FETCH_DECKS_FAILURE') {
    dispatch(setDeckListFetching(false));
    dispatch(setNotification(action.payload.errorMsg));
  }

  if (action.type === '[DECKLIST] FETCH_DECKS_SUCCEDED') {
    dispatch(setDeckListFetching(false));
    dispatch(setPagination(action.meta.newPage, action.meta.totalRecords, action.meta.reachedEnd));
    if (action.meta.context === 'initial') {
      dispatch(setDecks(action.payload));
      dispatch(setDeckListHasInitialLoad(true));
    } else if (action.meta.context === 'nextPage') {
      dispatch(appendDecks(action.payload));
    }
  }

  if (
    action.type === '[DECKLIST] SORT_PROPERTIES_CHANGED' ||
    action.type === '[DECKLIST] SEARCH_TERM_CHANGED'
  ) {
    dispatch(fetchInitialDeckList());
  }
};
