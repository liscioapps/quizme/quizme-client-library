import {
  appendPublicDecks,
  PublicDeckActions,
  fetchPublicDecksFailed,
  fetchPublicDecksSucceded,
  fetchInitialPublicDeckList,
  setPublicDeckListFetching,
  setPublicDeckListHasInitialLoad,
  setPublicDecks,
  setPublicDeckListNotification,
  setPublicDeckPagination,
} from "../actions/publicDecks.actions";
import { Index } from "../models/IndexHelper";
import { PublicDeckListState } from "../reducers/publicDeckList.reducer";
import { PublicDeckCRUDService } from "../services";
import { ReducerMiddleware } from "./reducerMiddleware";

export const publicDecksMiddleware: ReducerMiddleware<
  PublicDeckListState,
  PublicDeckActions
> = (getState, dispatch, next, action) => {
  next(action);
  const publicDecksService = PublicDeckCRUDService.Instance;

  if (action.type === "[PUBLIC_DECKLIST] INITIAL_FETCH_LIST") {
    const state = getState();
    if (state.isFetching) {
      // one request cycle at a time
      return;
    }

    dispatch(setPublicDeckListHasInitialLoad(false));
    dispatch(setPublicDeckListFetching(true));
    const { orderBy, orderDirection, pageSize } =
      state.currentPaginationSettings;
    const { searchTerm } = state;
    const otherFilters: Index<string> = {};

    if (searchTerm) {
      otherFilters["Title"] = `*${searchTerm}*`;
    }

    publicDecksService
      .getList(
        10,
        0,
        orderDirection === "desc" ? `-${orderBy}` : `${orderBy}`,
        [],
        otherFilters
      )
      .then((response) => {
        dispatch(
          fetchPublicDecksSucceded(
            response.data,
            response.resultPageIndex,
            response.resultTotals,
            response.didReachEnd,
            "initial"
          )
        ); // initial fetch success
      })
      .catch(() => {
        dispatch(fetchPublicDecksFailed("Faled to fetch decks"));
      });
  }

  if (action.type === "[PUBLIC_DECKLIST] FETCH_NEXT_LIST_PAGE") {
    const state = getState();
    if (state.isFetching) {
      // no duplicate requests
      return;
    }
    dispatch(setPublicDeckListFetching(true));

    const { orderBy, orderDirection, pageIndex, pageSize } =
      state.currentPaginationSettings;
    const { searchTerm } = state;

    const otherFilters: Index<string> = {};
    if (searchTerm) {
      otherFilters["Title"] = `*${searchTerm}*`;
    }
    publicDecksService
      .getList(
        pageSize,
        pageIndex + 1,
        orderDirection === "desc" ? `-${orderBy}` : `${orderBy}`,
        [],
        otherFilters
      )
      .then((response) => {
        dispatch(
          fetchPublicDecksSucceded(
            response.data,
            response.resultPageIndex,
            response.resultTotals,
            response.didReachEnd,
            "nextPage"
          )
        );
      })
      .catch(() => {
        dispatch(fetchPublicDecksFailed("Faled to fetch decks"));
      });
  }

  if (action.type === "[PUBLIC_DECKLIST] FETCH_DECKS_FAILURE") {
    dispatch(setPublicDeckListFetching(false));
    dispatch(setPublicDeckListNotification(action.payload.errorMsg));
  }

  if (action.type === "[PUBLIC_DECKLIST] FETCH_DECKS_SUCCEDED") {
    dispatch(setPublicDeckListFetching(false));
    dispatch(
      setPublicDeckPagination(
        action.meta.newPage,
        action.meta.totalRecords,
        action.meta.reachedEnd
      )
    );
    if (action.meta.context === "initial") {
      dispatch(setPublicDecks(action.payload));
      dispatch(setPublicDeckListHasInitialLoad(true));
    } else if (action.meta.context === "nextPage") {
      dispatch(appendPublicDecks(action.payload));
    }
  }

  if (action.type === "[PUBLIC_DECKLIST] SEARCH_TERM_CHANGED") {
    dispatch(fetchInitialPublicDeckList());
  }
};
