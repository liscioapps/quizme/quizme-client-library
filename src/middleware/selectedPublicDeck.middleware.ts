import {
  addToMyDecksFailed,
  addToMyDecksSucceeded,
  clearSelectedPublicDeckError,
  fetchSelectedPublicDeckFailed,
  fetchSelectedPublicDeckSucceeded,
  requestApprovalChangeFailed,
  SelectedPublicDeckActions,
  setSelectedPublicDeck,
  setSelectedPublicDeckApproved,
  setSelectedPublicDeckError,
  SetSelectedPublicDeckIsFetching,
  setSelectePublicDeckPrivateDeckId,
} from "../actions/selectedPublicDeck.actions";
import { SelectedPublicDeckState } from "../reducers/selectedPublicDeckReducer";
import { PublicDeckCRUDService } from "../services";
import { ReducerMiddleware } from "./reducerMiddleware";

export const selectedPublicDeckMiddleware: ReducerMiddleware<
  SelectedPublicDeckState,
  SelectedPublicDeckActions
> = (getState, dispatch, next, action) => {
  console.log("dispatch: ", action);
  next(action);
  const publicDecksService = PublicDeckCRUDService.Instance;

  if (action.type === "[SELECTED_PUBLIC_DECK] FETCH_PUBLIC_DECK") {
    const state = getState();

    dispatch(SetSelectedPublicDeckIsFetching(true));

    publicDecksService
      .getItem(action.payload.publicDeckId)
      .then((response) => {
        dispatch(fetchSelectedPublicDeckSucceeded(response)); // initial fetch success
      })
      .catch((e) => {
        dispatch(fetchSelectedPublicDeckFailed(e.message));
      });
  }

  if (action.type === "[SELECTED_PUBLIC_DECK] FETCH_DECK_FAILURE") {
    const state = getState();
    if (state.isFetching) {
      // one request cycle at a time
      return;
    }

    dispatch(setSelectedPublicDeckError(action.payload.errorMsg));
    dispatch(SetSelectedPublicDeckIsFetching(false));
  }

  if (action.type === "[SELECTED_PUBLIC_DECK] FETCH_SUCCEDED") {
    const state = getState();

    dispatch(setSelectedPublicDeck(action.payload));
    dispatch(setSelectePublicDeckPrivateDeckId())
    dispatch(SetSelectedPublicDeckIsFetching(false));
  }

  if (action.type === "[SELECTED_PUBLIC_DECK] ADD_TO_MY_DECKS") {
    dispatch(SetSelectedPublicDeckIsFetching(true));

    publicDecksService
      .addToMyDecks(action.payload.publicDeckId)
      .then((response) => {
        dispatch(addToMyDecksSucceeded(response));
      })
      .catch((e) => {
        dispatch(addToMyDecksFailed(e.message));
      });
  }

  if (action.type === "[SELECTED_PUBLIC_DECK] ADD_TO_MY_DECKS_SUCCEEDED") {
    dispatch(setSelectePublicDeckPrivateDeckId(action.payload.privateDeckId));
  }

  if (action.type === "[SELECTED_PUBLIC_DECK] REQUEST APPROVE") {
    dispatch(clearSelectedPublicDeckError());
    const state = getState();
    publicDecksService
      .approveDeck(state.selectedPublicDeck.id!)
      .then(() => {
        dispatch(setSelectedPublicDeckApproved(true));
      })
      .catch((error) => {
        dispatch(requestApprovalChangeFailed(error.message));
      });
  }

  if (action.type === "[SELECTED_PUBLIC_DECK] REQUEST UNAPPROVE") {
    dispatch(clearSelectedPublicDeckError());
    const state = getState();
    publicDecksService
      .unapproveDeck(state.selectedPublicDeck.id!)
      .then(() => {
        dispatch(setSelectedPublicDeckApproved(false));
      })
      .catch((error) => {
        dispatch(requestApprovalChangeFailed(error.message));
      });
  }

  if (action.type === "[SELECTED_PUBLIC_DECK] REQUEST APPROVAL CHANGE FAILED") {
    dispatch(setSelectedPublicDeckError(action.payload.message));
  }
};
