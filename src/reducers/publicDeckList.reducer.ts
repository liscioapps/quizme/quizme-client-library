import { Reducer } from '@reduxjs/toolkit';
import { PublicDeckActions } from '../actions/publicDecks.actions';
import { PaginationModel } from '../models';
import { produce } from 'immer';
import { PublicDeckSummaryModel } from '../models/PublicDeckSummaryModel';

export interface PublicDeckListState {
  publicDecksList: PublicDeckSummaryModel[];
  currentPaginationSettings: PaginationModel;
  searchTerm: string;
  totalDecks?: number;
  hasMoreDecks: boolean;
  hasInitialLoad: boolean;
  isFetching: boolean;
  pageCurrentlyFetching: number;
}

export const initialPublicDeckState: PublicDeckListState = {
  publicDecksList: [],
  currentPaginationSettings: {
    pageIndex: 0,
    pageSize: 2,
    orderBy: 'CreatedDate',
    orderDirection: 'desc',
  },
  searchTerm: '',
  hasMoreDecks: false,
  hasInitialLoad: false,
  isFetching: false,
  pageCurrentlyFetching: 0,
};

export const publicDeckReducer: Reducer<PublicDeckListState, PublicDeckActions> = produce(
  (state, action) => {
    // I don't think state should be possibly undefined?
    if (undefined === state) {
      return initialPublicDeckState;
    }

    switch (action.type) {
      case '[PUBLIC_DECKLIST] SET_FETCHING':
        state.isFetching = action.payload;
        break;
      case '[PUBLIC_DECKLIST] SET_HAS_INITIAL_LOAD':
        state.hasInitialLoad = action.payload;
        break;
      case '[PUBLIC_DECKLIST] SET_DECKS':
        state.publicDecksList = action.payload;
        break;
      case '[PUBLIC_DECKLIST] APPEND_DECKS':
        state.publicDecksList.push(...action.payload);
        break;
      case '[PUBLIC_DECKLIST] SET_PAGE_SIZE':
        state.currentPaginationSettings.pageSize = action.payload;
        break;
      case '[PUBLIC_DECKLIST] SET_PAGINATION':
        state.currentPaginationSettings.pageIndex = action.payload.currentPage;
        state.totalDecks = action.payload.totalRecords;
        state.hasMoreDecks = !action.payload.didReachEnd;
        break;
      case '[PUBLIC_DECKLIST] SEARCH_TERM_CHANGED':
        state.searchTerm = action.payload;
        break;
      default:
        return state;
    }
  }
);
