import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { HttpClientService } from '../services/HttpClientService';
import jwt from 'jwt-decode';
import { SubscriptionPlanSettings } from '../models/PlanSettings';
import { SubscriptionClaimKeys } from '../models/SubscriptionClaimsKeys';

const apiClientService: HttpClientService = HttpClientService.Instance;

export interface AuthState {
  userLoggedIn: boolean;
  authorizationToken: string | null;
  isAdmin: boolean;
  subscriptionPlanId: number;
  subscriptionSettings: SubscriptionPlanSettings;
  subscriptionIsExpired: boolean;
  refreshToken: string | null;
  tokenExpiresAt: number | null;
  tokenExpired: any;
  userInformation?: any;
  userHasOnboarded: boolean;
}

const initialState: AuthState = {
  userLoggedIn: false,
  isAdmin: false,
  subscriptionPlanId: 0,
  subscriptionSettings: {
    numberOfDecksAllowed: 0,
    analyticsEnabled: false,
    assignDecksToOtherUser: false,
    numberOfCardsAllowed: 0,
  },
  subscriptionIsExpired: false,
  authorizationToken: null,
  refreshToken: null,
  tokenExpiresAt: null,
  tokenExpired: null,
  userInformation: null,
  userHasOnboarded: true, // better to assume true here in case some weirdness happens on init
};

const getPlanSettings = (decodedToken: any): SubscriptionPlanSettings => {
  const settingsString = decodedToken[
    SubscriptionClaimKeys.SubscriptionSettingPrefixClaimKey
  ] as string;
  return JSON.parse(settingsString) as SubscriptionPlanSettings;
};

const checkSubscriptionIsExpired = (decodedToken: any): boolean => {
  const unixDateString = decodedToken[SubscriptionClaimKeys.SubscriptionExpiration] as string;
  const expiration = new Date(Number(unixDateString));
  return expiration < new Date();
};

const checkIsAdmin = (tokenPayload: any): boolean => {
  const roles = tokenPayload['http://schemas.microsoft.com/ws/2008/06/identity/claims/role'];
  if (typeof roles === 'string' && roles === 'ADMIN') {
    return true;
  }
  if (Array.isArray(roles) && roles.findIndex((r) => r === 'ADMIN') !== -1) {
    return true;
  }
  return false;
};

export const authReducerSlice = createSlice({
  name: 'Auth',
  initialState,
  reducers: {
    storeAuthToken: (state: AuthState, action: PayloadAction<string>) => {
      state.authorizationToken = action.payload;
      if (state.authorizationToken) {
        state.userLoggedIn = true;
        apiClientService.setDefaultHeader('authorization', state.authorizationToken);

        const decodedToken = jwt<any>(state.authorizationToken);

        state.isAdmin = checkIsAdmin(decodedToken) as any;
        state.subscriptionPlanId = decodedToken[SubscriptionClaimKeys.SubscriptionPlanIdClaimKey];
        state.subscriptionSettings = getPlanSettings(decodedToken);
        state.subscriptionIsExpired = checkSubscriptionIsExpired(decodedToken);
      } else {
        apiClientService.setDefaultHeader('authorization', null);
      }
    },

    setTokenExpiresAt: (state: AuthState, action: PayloadAction<number>) => {
      state.tokenExpiresAt = action.payload;
    },

    setTokenExpired: (state: AuthState, action: PayloadAction<any>) => {
      state.tokenExpired = action.payload;
    },

    storeRefreshToken: (state: AuthState, action: PayloadAction<string>) => {
      state.refreshToken = action.payload;
    },
    setUserHasOnboarded: (state: AuthState, action: PayloadAction<boolean>) => {
      state.userHasOnboarded = action.payload;
    },
    storeUserInformation: (state: AuthState, action: PayloadAction<any>) => {
      state.userInformation = action.payload;
    },
    logout: (state) => {
      state.userLoggedIn = false;
      state.authorizationToken = null;
      state.tokenExpired = true;
      state.tokenExpiresAt = null;
      state.userInformation = null;
    },
  },
});

export const {
  storeAuthToken,
  storeRefreshToken,
  logout,
  setTokenExpiresAt,
  setTokenExpired,
  storeUserInformation,
  setUserHasOnboarded,
} = authReducerSlice.actions;
export const authReducer = authReducerSlice.reducer;
