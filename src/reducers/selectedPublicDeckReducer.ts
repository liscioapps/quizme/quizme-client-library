import { Reducer } from "@reduxjs/toolkit";
import { produce } from "immer";
import { PublicDeckModel } from "../models/PublicDeckModel";
import { SelectedPublicDeckActions } from "../actions/selectedPublicDeck.actions";

export interface SelectedPublicDeckState {
  selectedPublicDeck: PublicDeckModel;
  isFetching: boolean;
  privateDeckId?: number;
  hasError?: boolean;
  errorMessage?: string;
}

const initialSelectedPublicDeckState: SelectedPublicDeckState = {
  selectedPublicDeck: {
    description: "",
    title: "",
    approved: false,
    cards: [],
  },
  isFetching: false,
};

export const selectedPublicDeckReducer: Reducer<
  SelectedPublicDeckState,
  SelectedPublicDeckActions
> = produce((state, action) => {
  // I don't think state should be possibly undefined?
  if (undefined === state) {
    return initialSelectedPublicDeckState;
  }

  switch (action.type) {
    case "[SELECTED_PUBLIC_DECK] SET_DECK":
      state.selectedPublicDeck = action.payload.deck;
      break;
    case "[SELECTED_PUBLIC_DECK] SET_IS_FETCHING":
      state.isFetching = action.payload.isFetching;
      break;
    case "[SELECTED_PUBLIC_DECK] SET_ERROR":
      state.hasError = true;
      state.errorMessage = action.payload.errorMessage;
      break;
    case "[SELECTED_PUBLIC_DECK] CLEAR_ERROR":
      state.hasError = false;
      state.errorMessage = "";
      break;
    case "[SELECTED_PUBLIC_DECK] SET_PRIVATE_DECK_ID":
      state.privateDeckId = action.payload.privateDeckId;
      break;
    case "[SELECTED_PUBLIC_DECK] SET APPROVED":
      state.selectedPublicDeck.approved = action.payload.approved;
      break;
    default:
      return state;
  }
});
