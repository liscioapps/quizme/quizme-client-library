import { Reducer } from '@reduxjs/toolkit';
import { DeckActions } from '../actions/decks.actions';
import { DeckSummaryModel, PaginationModel } from '../models';
import { produce } from 'immer';

export interface DeckListState {
  decksList: DeckSummaryModel[];
  currentPaginationSettings: PaginationModel;
  searchTerm: string;
  totalDecks?: number;
  hasMoreDecks: boolean;
  hasInitialLoad: boolean;
  isFetching: boolean;
  pageCurrentlyFetching: number;
}

export const initialDeckState: DeckListState = {
  decksList: [],
  currentPaginationSettings: {
    pageIndex: 0,
    pageSize: 30,
    orderBy: 'DueCardCount',
    orderDirection: 'desc',
  },
  searchTerm: '',
  hasMoreDecks: false,
  hasInitialLoad: false,
  isFetching: false,
  pageCurrentlyFetching: 0,
};

export const deckReducer: Reducer<DeckListState, DeckActions> = produce((state, action) => {
  // I don't think state should be possibly undefined?
  if (undefined === state) {
    return initialDeckState;
  }

  switch (action.type) {
    case '[DECKLIST] SET_FETCHING':
      state.isFetching = action.payload;
      break;
    case '[DECKLIST] SET_HAS_INITIAL_LOAD':
      state.hasInitialLoad = action.payload;
      break;
    case '[DECKLIST] SET_DECKS':
      state.decksList = action.payload;
      break;
    case '[DECKLIST] APPEND_DECKS':
      state.decksList.push(...action.payload);
      break;
    case '[DECKLIST] SET_ORDER_BY_ASCENDING':
      state.currentPaginationSettings.orderBy = action.payload.field;
      state.currentPaginationSettings.orderDirection = 'asc';
      break;
    case '[DECKLIST] SET_ORDER_BY_DESCENDING':
      state.currentPaginationSettings.orderBy = action.payload.field;
      state.currentPaginationSettings.orderDirection = 'desc';
      break;
    case '[DECKLIST] SET_PAGE_SIZE':
      state.currentPaginationSettings.pageSize = action.payload;
      break;
    case '[DECKLIST] SET_PAGINATION':
      state.currentPaginationSettings.pageIndex = action.payload.currentPage;
      state.totalDecks = action.payload.totalRecords;
      state.hasMoreDecks = !action.payload.didReachEnd;
      break;
    case '[DECKLIST] SORT_PROPERTIES_CHANGED':
      state.currentPaginationSettings.orderBy = action.payload.field;
      state.currentPaginationSettings.orderDirection = action.payload.direction;
      break;
    case '[DECKLIST] SEARCH_TERM_CHANGED':
      state.searchTerm = action.payload;
      break;
    default:
      return state;
  }
});
