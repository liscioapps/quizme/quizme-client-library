import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { CardModel, DeckModel } from "../models";

export interface SelectedDeckState {
  currentDeck: DeckModel;
}

export const initialSelectedDeckState: SelectedDeckState = {
  currentDeck: {
    __id: "",
    title: "",
    description: "",
    cards: [],
  },
};

export const reducer = createSlice({
  name: "Selected Deck",
  initialState: initialSelectedDeckState,
  reducers: {
    setCurrentDeck: (
      state: SelectedDeckState,
      action: PayloadAction<DeckModel>
    ) => {
      state.currentDeck = action.payload;
    },
    clearCurrentDeck: (state: SelectedDeckState) => {
      state.currentDeck = initialSelectedDeckState.currentDeck;
    },
    updateCurrentDeckDescription: (
      state: SelectedDeckState,
      action: PayloadAction<{ title: string; description: string }>
    ) => {
      state.currentDeck.title = action.payload.title;
      state.currentDeck.description = action.payload.description;
    },
    addCardToCurrentDeck: (
      state: SelectedDeckState,
      action: PayloadAction<CardModel>
    ) => {
      if (state.currentDeck.cards?.length) {
        state.currentDeck.cards.unshift(action.payload);
      } else {
        state.currentDeck.cards = [action.payload];
      }

      state.currentDeck.dueCardCount = state.currentDeck.dueCardCount
        ? state.currentDeck.dueCardCount + 1
        : 1;
    },
    updateCardInCurrentDeck: (
      state: SelectedDeckState,
      action: PayloadAction<CardModel>
    ) => {
      state.currentDeck.cards = state.currentDeck.cards
        ? (state.currentDeck.cards = state.currentDeck.cards.map((c) =>
            c.id === action.payload.id ? action.payload : c
          ))
        : [action.payload];
    },
    deleteCardFromCurrentDeck: (
      state: SelectedDeckState,
      action: PayloadAction<number>
    ) => {
      if (state.currentDeck.cards) {
        state.currentDeck.cards = state.currentDeck.cards.filter(
          (c) => c.id !== action.payload
        );
      }
    },
  },
});

export const {
  setCurrentDeck,
  clearCurrentDeck,
  updateCurrentDeckDescription,
  addCardToCurrentDeck,
  updateCardInCurrentDeck,
  deleteCardFromCurrentDeck,
} = reducer.actions;

export const selectedDeckReducer = reducer.reducer;
