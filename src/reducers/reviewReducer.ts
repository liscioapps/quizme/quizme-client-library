import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { CardModel } from "../models/CardModel";

export interface ReviewState {
  cardsToReview: CardModel[];
  currentCard: CardModel | null;
}

export const initialReviewState: ReviewState = {
  cardsToReview: [],
  currentCard: null,
};

export const reviewReducerSlice = createSlice({
  name: "Review",
  initialState: initialReviewState,
  reducers: {
    /**
     * Sets the cards the user wants to review
     */
    setCardsToReview: (
      state: ReviewState,
      action: PayloadAction<CardModel[]>
    ) => {
      state.cardsToReview = action.payload;
      state.currentCard = action.payload?.[0];
    },
    /**
     * Marks previous card as reviewed, sets the next card to review
     */
    moveToNextCard: (state: ReviewState) => {
      state.cardsToReview.shift();
      state.currentCard = state.cardsToReview.length
        ? state.cardsToReview[0]
        : null;
    },
  },
});

export const { setCardsToReview, moveToNextCard } = reviewReducerSlice.actions;
export const reviewReducer = reviewReducerSlice.reducer;
