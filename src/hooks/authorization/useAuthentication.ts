/**
 * Define the hook used for authenticating
 */
import { AuthService } from "../../services/AuthService";
import {
  AuthenticationResultModel,
  ResetPasswordModel,
} from "../../models/AuthenticationResultModel";
import {
  storeRefreshToken,
  storeAuthToken,
  setTokenExpiresAt,
  setTokenExpired,
  logout,
  storeUserInformation,
} from "../../reducers/authReducer";

export function useAuthentication(dispatcher?: Function) {
  const authService = AuthService.Instance;

  return {
    login: async (
      username: string,
      password: string
    ): Promise<AuthenticationResultModel> => {
      const response: AuthenticationResultModel = await authService.logIn(
        username,
        password
      );

      if (undefined !== dispatcher) {
        dispatcher(storeRefreshToken(response.refreshToken));
        dispatcher(storeAuthToken(response.token));
        dispatcher(setTokenExpiresAt(response.tokenExpiresAt));
        dispatcher(setTokenExpired(false));
      }

      return response;
    },
    refreshAuthToken: async (
      timezone: string,
      refreshToken?: string
    ): Promise<AuthenticationResultModel> => {
      const response: AuthenticationResultModel =
        await authService.refreshAuthToken(timezone, refreshToken);
      if (undefined !== dispatcher) {
        dispatcher(storeRefreshToken(response.refreshToken));
        dispatcher(storeAuthToken(response.token));
        dispatcher(setTokenExpiresAt(response.tokenExpiresAt));
        dispatcher(setTokenExpired(false));
      }

      return response;
    },
    signup: async (
      username: string,
      password: string
    ): Promise<AuthenticationResultModel> => {
      const response: AuthenticationResultModel = await authService.signUp({
        userName: username,
        password,
      });

      if (undefined !== dispatcher) {
        dispatcher(storeRefreshToken(response.refreshToken));
        dispatcher(storeAuthToken(response.token));
        dispatcher(setTokenExpiresAt(response.tokenExpiresAt));
        dispatcher(setTokenExpired(false));
      }

      return response;
    },

    requestForgotPassword: async (userName: string): Promise<any> => {
      const response: any = await authService.requestResetPassword(userName);
      return response;
    },

    requestResetPasswordToken: async (): Promise<any> => {
      const response: any = await authService.requestResetPasswordToken();
      return response;
    },

    resetPassword: async (
      password: string,
      resetPasswordToken: string,
      oldPassword: string | null = null
    ): Promise<any> => {
      const response: any = await authService.resetPassword(<
        ResetPasswordModel
      >{
        oldPassword: oldPassword,
        newPassword: password,
        confirmPassword: password,
        changePasswordVerificationCode: resetPasswordToken,
      });

      return response;
    },

    loadUserInformation: async () => {
      const response = await authService.loadUserProfile();

      if (undefined !== dispatcher) {
        dispatcher(storeUserInformation(response));
      }

      return response;
    },
    logout: async (alreadyLoggedOutFromServer = false) => {
      await authService.logout(alreadyLoggedOutFromServer);
      if (undefined !== dispatcher) {
        dispatcher(logout());
      }
    },
  };
}
