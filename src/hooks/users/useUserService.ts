import { UserCRUDApiService } from "../../services/UserCRUDApiService";

export const useUserService = () => {
  const dataService = UserCRUDApiService.Instance;

  const deleteCurrentUserAccount = async () => {
    return await dataService.deleteCurrentUserAccount();
  };

  return {
    deleteCurrentUserAccount,
  };
};
