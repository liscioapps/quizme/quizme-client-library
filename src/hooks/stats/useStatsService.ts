import { useState } from 'react';
import { HomeDashboardStats } from '../../models/HomeDashboardStats';
import { StatsService } from '../../services/StatsService';

export function useStatsService() {
  const dataService = StatsService.Instance;
  const [loading, setLoading] = useState<boolean>(false);

  async function getHomepageDashboard(): Promise<HomeDashboardStats> {
    setLoading(true);

    try {
      const response = await dataService.getHomepageDashboard();

      return response;
    } finally {
      setLoading(false);
    }
  }

  return {
    isLoading: loading,
    getHomepageDashboard,
  };
}
