import { useState } from "react";
import {
  DeckListState,
  initialDeckState,
} from "../../reducers/deckList.reducer";
import { PaginationModel } from "../../models/PaginationModel";
import { CardCRUDService } from "../../services/CardCRUDService";
import { CardModel } from "../../models/CardModel";
import { quizMeConfig } from "../../config";

export function useCardService(
  dispatcher?: Function,
  deckState?: DeckListState
) {
  const dataService: CardCRUDService = CardCRUDService.Instance;
  deckState = deckState || initialDeckState;
  const [loading, setLoading] = useState<boolean>(false);

  async function getList(
    deckId: number,
    refresh: boolean = true,
    columnsToRetrieve: string[] = [],
    otherFilters = {},
    paginationSettings: PaginationModel = deckState?.currentPaginationSettings!
  ): Promise<CardModel[]> {
    setLoading(true);

    const shouldRefresh = refresh || deckState?.decksList.length === 0;

    let pageIndex = shouldRefresh ? 0 : paginationSettings.pageIndex! + 1;

    const decksListResponse = await dataService.getCardsForDeck(
      deckId,
      paginationSettings.pageSize,
      pageIndex,
      `-ModifiedDate`, // hard coding this for now - we don't support changing it
      columnsToRetrieve,
      otherFilters
    );

    setLoading(false);

    return decksListResponse;
  }

  async function getCard(id: number, columns: string[] = []) {
    setLoading(true);

    const response = await dataService.getItem(id, columns);

    setLoading(false);

    return response;
  }

  async function createCard(deckModel: CardModel) {
    setLoading(true);

    const deckCreationResponse = await dataService.createItem(deckModel);

    setLoading(false);

    return deckCreationResponse;
  }

  async function updateCard(id: number, model: CardModel) {
    setLoading(true);

    const response = await dataService.updateItem(id, model);

    setLoading(false);

    return response;
  }

  async function deleteCard(id: number) {
    setLoading(true);

    const response = await dataService.deleteItem(id);

    setLoading(false);

    return response;
  }

  async function reviewCard(id: number, score: 1 | 2 | 3 | 4 | 5) {
    setLoading(true);
    await dataService.reviewCard(id, score);
    setLoading(false);
  }

  async function getDueCardsByDeck(deckId: number) {
    setLoading(true);
    const response = await dataService.getDueCardsByDeck(deckId);
    setLoading(false);
    return response;
  }

  async function getAllDueCardsByUser() {
    setLoading(true);
    const response = await dataService.getAllDueCardsByUser();
    setLoading(false);
    return response;
  }

  async function importCardsCsv(deckId: number, csvFile: File) {
    const response = await dataService.importCardsCsv(deckId, csvFile);
    return response;
  }

  async function getExampleCsv() {
    return await dataService.getCsvExampleTemplate();
  }

  return {
    isLoading: loading,

    getList,

    getCard,
    createCard,
    updateCard,
    deleteCard,
    reviewCard,
    getDueCardsByDeck,
    getAllDueCardsByUser,
    importCardsCsv,
    getExampleCsv,
  };
}
