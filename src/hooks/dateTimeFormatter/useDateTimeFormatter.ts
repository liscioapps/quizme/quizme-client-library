import { DateTime } from 'luxon';

export function useDateTimeFormatter() {
  return {
    toReadableDateTime(dateTime: Date): string {
      return DateTime.fromJSDate(dateTime).toFormat('MM/dd/yyyy tt');
    },
    toReadableDayPart(dateTime: Date): string {
      return DateTime.fromJSDate(dateTime).toFormat('MM/dd/yyyy');
    },
    getTimeBetween(date1: Date, date2: Date) {
      const first = DateTime.fromJSDate(new Date(date1.toDateString()));
      let second = DateTime.fromJSDate(new Date(date2.toDateString()));
      const diff = second
        .diff(first, ['years', 'months', 'days'], {
          conversionAccuracy: 'longterm',
        })
        .toObject();

      const round = (number: number) => {
        return number < 0 ? Math.ceil(number) : Math.floor(number);
      };
      const days = round(diff.days ?? 0);
      const months = round(diff.months ?? 0);
      const years = round(diff.years ?? 0);

      const format = (amount: number, unit: string) => {
        const main = `${Math.abs(amount)} ${unit}${Math.abs(amount) > 1 ? 's' : ''}`;
        const withPastTense = amount < 0 ? main + ' ago' : main;
        return withPastTense;
      };

      if (years) {
        return format(years, 'year');
      }
      if (months) {
        return format(months, 'month');
      }
      if (days) {
        return format(days, 'day');
      }
      return 'today';
    },
  };
}
