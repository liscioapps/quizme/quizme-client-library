import { SubscriptionsService } from "../../services/SubscriptionsService";

export function useSubscriptionsService() {
  const subscriptionsService: SubscriptionsService =
    SubscriptionsService.Instance;

  async function getUserSubscription() {
    const subscription = await subscriptionsService.getUserSubscription();
    return subscription;
  }

  async function getStripeCheckoutLink(stripePriceId: string) {
    const link = await subscriptionsService.getStripeCheckoutLink(
      stripePriceId
    );
    return link;
  }

  async function getStripePortalLink() {
    const link = await subscriptionsService.getStripePortalLink();
    return link;
  }

  return {
    getUserSubscription,
    getStripeCheckoutLink,
    getStripePortalLink,
  };
}
