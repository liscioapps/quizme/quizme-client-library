import { UserEventsService } from '../../services/UserEventsService';

export function useUserEventsService() {
  const userEventsService: UserEventsService = UserEventsService.Instance;

  async function getOnboardCompleteStatus() {
    const subscription = await userEventsService.getOnboardCompleteStatus();
    return subscription;
  }

  async function setOnboardedComplete(agent: 'web' | 'mobile') {
    await userEventsService.setOnboardCompleteStatus(agent);
  }

  return {
    getOnboardCompleteStatus,
    setOnboardedComplete,
  };
}
