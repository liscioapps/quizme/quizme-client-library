import { UserNotificationSettingModel } from "../../models/UserNotificationSettingModel";
import { UserNotificationSettingService } from "../../services/UserNotificationSettingService";

export const useUserNotificationSettings = () => {
  const dataService = UserNotificationSettingService.Instance;

  const getUserDueDeckNotificationSettings = async () => {
    return await dataService.getUserDueDeckNotificationSettings();
  };

  const updateUserDueDeckNotificationSettings = async (
    settings: UserNotificationSettingModel
  ) => {
    return await dataService.updateUserDueDeckNotificationSettings(settings);
  };

  return {
    getUserDueDeckNotificationSettings,
    updateUserDueDeckNotificationSettings,
  };
};
