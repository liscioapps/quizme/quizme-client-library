import { DeckCRUDService } from "../../services/DeckCRUDService";
import { DeckSummaryModel } from "../../models/DeckSummaryModel";
import { useState } from "react";
import {
  DeckListState,
  initialDeckState,
} from "../../reducers/deckList.reducer";

export function useDeckService(
  dispatcher?: Function,
  deckState?: DeckListState
) {
  const dataService: DeckCRUDService = DeckCRUDService.Instance;
  deckState = deckState || initialDeckState;
  const [loadingLegacy, setLoading] = useState<boolean>(false);

  async function createDeck(deckModel: DeckSummaryModel) {
    setLoading(true);

    const deckCreationResponse = await dataService.createItem(deckModel);

    setLoading(false);

    return deckCreationResponse;
  }

  async function getDeck(id: number, columns: string[] = []) {
    setLoading(true);

    const response = await dataService.getItem(id, columns);

    setLoading(false);

    return response;
  }

  async function getDeckDetails(id: number) {
    setLoading(true);

    const response = await dataService.getDetails(id);

    setLoading(false);

    return response;
  }

  async function updateDeck(id: number, model: DeckSummaryModel) {
    setLoading(true);

    const response = await dataService.updateItem(id, model);

    setLoading(false);

    return response;
  }

  async function deleteDeck(id: number) {
    setLoading(true);

    const response = await dataService.deleteItem(id);

    setLoading(false);

    return response;
  }

  return {
    loadingLegacy,

    /**
     * Retrieves list of decks
     * @param refresh Indicates that the request is to refresh entire list of decks. Set to false to append to current list
     * @param columnsToRetrieve list of properties belong to deck entity to retrieve
     * @param otherFilters Other filters
     * @returns [awaitable] List of decks returned from this request only.
     */
    // getList,

    /**
     * Refreshes the entire decks list
     */
    // refreshDecksList,
    getDeck,
    getDeckDetails,
    createDeck,
    updateDeck,
    deleteDeck,
  };
}
