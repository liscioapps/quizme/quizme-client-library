interface StorageKeys {
  userLoggedin: string;
  authorizationToken: string;
  refreshToken: string;
}

export const storageKeys: StorageKeys = {
  userLoggedin: 'USER_LOGGED_IN',
  authorizationToken: 'AUTHORIZATION_TOKEN',
  refreshToken: 'REFRESH_TOKEN'
};

