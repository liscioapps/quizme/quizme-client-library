import { BaseEntityModel } from "./BaseEntityModel";

export interface PublicDeckSummaryModel extends BaseEntityModel {
  title: string;
  description: string;
  approved: boolean;
}
