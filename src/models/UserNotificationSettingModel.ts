export interface UserNotificationSettingModel {
  userId: number;
  receiveViaEmail: boolean;
  receiveViaSms: boolean;
  receiveViaPushNotification: boolean;
}
