import { DeckSummaryModel } from "./DeckSummaryModel";

export interface HomeDashboardStats {
  streak: number;
  recentDecks: DeckSummaryModel[];
  dueCardsCount: number;
  dueDecksCount: number;
  nextDue: string | null;
}
