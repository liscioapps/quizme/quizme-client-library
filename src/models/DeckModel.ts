import { BaseEntityModel } from './BaseEntityModel';
import { CardModel } from './CardModel';

export interface DeckModel extends BaseEntityModel {
  __id: string;
  title: string;
  description: string;
  userId?: number;
  dueCardCount?: number;
  progressPercentage?: number;
  cards?: CardModel[];
}
