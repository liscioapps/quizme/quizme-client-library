export interface PaginationModel {
  pageSize: number;
  pageIndex: number;
  orderBy: string;
  orderDirection: "asc" | "desc";
}
