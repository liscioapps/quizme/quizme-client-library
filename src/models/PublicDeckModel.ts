import { BaseEntityModel } from "./BaseEntityModel";
import { PublicCardModel } from "./PublicCardModel";

export interface PublicDeckModel extends BaseEntityModel {
  title: string;
  description: string;
  approved: boolean;
  cards: PublicCardModel[];
}
