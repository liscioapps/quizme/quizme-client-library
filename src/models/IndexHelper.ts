export interface Index<T> {
  [name: string]: T;
}
