export interface SubscriptionPlanSettings {
  numberOfDecksAllowed: number;
  numberOfCardsAllowed: number;
  assignDecksToOtherUser: boolean;
  analyticsEnabled: boolean;
}
