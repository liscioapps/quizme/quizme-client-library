import { quizMeConfig } from "../config";
import HttpClientService from "./HttpClientService";

export class UserCRUDApiService {
  protected httpClient: HttpClientService;
  protected baseURL: string = "api/user";

  private static _instance: UserCRUDApiService;

  static get Instance(): UserCRUDApiService {
    return (
      UserCRUDApiService._instance ||
      (UserCRUDApiService._instance = new UserCRUDApiService())
    );
  }

  constructor() {
    if (
      undefined === quizMeConfig.storageService ||
      null === quizMeConfig.storageService
    ) {
      throw new Error(
        "quizMeConfig.storageService must be registered before application starts"
      );
    }
    this.httpClient = HttpClientService.Instance;
  }

  async deleteCurrentUserAccount(): Promise<void> {
    const response = await this.httpClient.delete(
      `${quizMeConfig.apiEndpointUrl}/${this.baseURL}`,
      {
        headers: {
          Accept: "application/json",
        },
      }
    );
  }
}
