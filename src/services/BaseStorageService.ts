import localforage from 'localforage';

export interface BaseStorageService {
  store(key: string, value: any): Promise<any>;
  retrieve(key: string): Promise<any>;
  contains(key: string): Promise<boolean>;
  remove(key: string): Promise<any>;
  clear(): Promise<any>;
}

export class LocalforageStorageService implements BaseStorageService {
  static _instance: LocalforageStorageService;

  static get Instance(): LocalforageStorageService {
    return LocalforageStorageService._instance || (LocalforageStorageService._instance = new LocalforageStorageService());
  }

  private storage: LocalForage;

  constructor() {
    const storageName = 'liscioApps-Quizme';

    localforage.config({
      driver: [localforage.INDEXEDDB, localforage.LOCALSTORAGE],
      name: storageName,
      version: 1.0,
      storeName: storageName, // Should be alphanumeric, with underscores.
      description: `Storage for ${storageName}`
    });

    this.storage = localforage;
  }
  
  async contains(key: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.storage.getItem(key, (error, value) => {
        if (error) {
          reject(error);
          return;
        }
        resolve(!!value);
      });
    });
  }

  async retrieve(key: string) : Promise<any> {
    return new Promise((resolve, reject) => {
      this.storage.getItem(key, (error, value) => {
        if (error) {
          reject(error);
          return;
        }
        resolve(value);
      });
    });
  }

  async store(key: string, value: any) : Promise<any> {
    return new Promise((resolve, reject) => {
      this.storage.setItem(key, value, (error, v) => {
        if (error) {
          reject(error);
          return;
        }
        resolve(true);
      });
    });
  }

  async remove(key: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.storage.removeItem(key, (error) => {
        if (error) {
          reject(error);
          return;
        }
        resolve(true);
      });
    });
  }

  async clear(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.storage.clear((error) => {
        if (error) {
          reject(error);
          return;
        }
        resolve(true);
      });
    });
  }
}