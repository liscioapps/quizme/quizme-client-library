import { DeckSummaryModel } from "../models/DeckSummaryModel";
import { BaseCRUDApiClient } from "./BaseCRUDApiClient";
import { quizMeConfig } from "../config";

export class DeckCRUDService extends BaseCRUDApiClient<DeckSummaryModel> {
  protected baseURL: string = "api/Decks";

  private static _instance: DeckCRUDService;

  static get Instance(): DeckCRUDService {
    return (
      DeckCRUDService._instance ||
      (DeckCRUDService._instance = new DeckCRUDService())
    );
  }

  async getDetails(itemId: number): Promise<DeckSummaryModel> {
    const response: DeckSummaryModel = await this.httpClient
      .get<DeckSummaryModel>(
        `${quizMeConfig.apiEndpointUrl}/${this.baseURL}/${itemId}/details`,
        {
          headers: {
            Accept: "application/json",
          },
        }
      )
      .then((_) => _.data);

    return response;
  }
}
