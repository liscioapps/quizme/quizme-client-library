import { quizMeConfig } from "./../config";
import { CardModel } from "../models/CardModel";
import { BaseCRUDApiClient } from "./BaseCRUDApiClient";

export class CardCRUDService extends BaseCRUDApiClient<CardModel> {
  protected baseURL: string = "api/Cards";

  private static _instance: CardCRUDService;

  static get Instance(): CardCRUDService {
    return (
      CardCRUDService._instance ||
      (CardCRUDService._instance = new CardCRUDService())
    );
  }

  async getCardsForDeck(
    deckId: number,
    pageSize: number = 20,
    pageIndex: number = 0,
    orderBy: string = "",
    columns: string[] = [],
    otherFilters: any = null
  ): Promise<CardModel[]> {
    const response: CardModel[] = await this.httpClient
      .get<CardModel[]>(
        `${quizMeConfig.apiEndpointUrl}/api/Decks/${deckId}/cards`,
        {
          headers: {
            Accept: "application/json",
          },
          params: {
            pageIndex,
            pageSize,
            orderBy: orderBy || null,
            columns: columns.length ? columns.join(",") : null,
            ...(otherFilters || {}),
          },
        }
      )
      .then((_) => _.data);

    return response;
  }

  async reviewCard(cardId: number, score: 1 | 2 | 3 | 4 | 5): Promise<void> {
    await this.httpClient.post<void>(
      `${quizMeConfig.apiEndpointUrl}/api/Answers`,
      { cardId, answer: score },
      {
        headers: {
          Accept: "application/json",
        },
      }
    );
  }

  async getDueCardsByDeck(deckId: number): Promise<CardModel[]> {
    const response: CardModel[] = await this.httpClient
      .get<CardModel[]>(
        `${quizMeConfig.apiEndpointUrl}/api/Decks/${deckId}/dueCards`,
        {
          headers: {
            Accept: "application/json",
          },
        }
      )
      .then((_) => _.data);

    return response;
  }

  async getAllDueCardsByUser(): Promise<CardModel[]> {
    const response: CardModel[] = await this.httpClient
      .get<CardModel[]>(`${quizMeConfig.apiEndpointUrl}/api/cards/due`, {
        headers: {
          Accept: "application/json",
        },
      })
      .then((_) => _.data);

    return response;
  }

  async importCardsCsv(deckId: number, csvFile: File) {
    const formData = new FormData();
    formData.append("file", csvFile);
    return await this.httpClient
      .post<{ importedCardsCount: number }>(
        `${quizMeConfig.apiEndpointUrl}/api/cards/import/${deckId}`,
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      )
      .then((_) => _.data);
  }

  async getCsvExampleTemplate() {
    return await this.httpClient
      .get<Blob>(`${quizMeConfig.apiEndpointUrl}/api/cards/import/csvTemplate`)
      .then((_) => {
        const contentDisposition = _.headers["content-disposition"];
        const match = contentDisposition?.match(/filename\s*=\s*"(.+)"/i);
        const filename = (match?.[1] ?? "QuizMe_Import_Template.csv") as string;

        return {
          filename,
          data: _.data,
        };
      });
  }
}
