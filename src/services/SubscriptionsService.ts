import { quizMeConfig } from "../config";
import { UserSubscriptionModel } from "../models/SubscriptionModel";
import HttpClientService from "./HttpClientService";

export class SubscriptionsService {
  protected httpClient: HttpClientService;
  protected baseURL: string = "api/subscription";

  private static _instance: SubscriptionsService;

  static get Instance(): SubscriptionsService {
    return (
      SubscriptionsService._instance ||
      (SubscriptionsService._instance = new SubscriptionsService())
    );
  }

  constructor() {
    if (
      undefined === quizMeConfig.storageService ||
      null === quizMeConfig.storageService
    ) {
      throw new Error(
        "quizMeConfig.storageService must be registered before application starts"
      );
    }
    this.httpClient = HttpClientService.Instance;
  }

  async getUserSubscription(): Promise<UserSubscriptionModel> {
    const response = await this.httpClient.get<UserSubscriptionModel>(
      `${quizMeConfig.apiEndpointUrl}/${this.baseURL}`,
      {
        headers: {
          Accept: "application/json",
        },
      }
    );

    return response.data;
  }

  async getStripeCheckoutLink(stripePriceId: string): Promise<string> {
    const response = await this.httpClient.post<string>(
      `${quizMeConfig.apiEndpointUrl}/${this.baseURL}/stripeCheckout/${stripePriceId}`,
      {
        headers: {
          Accept: "application/json",
        },
      }
    );

    return response.data;
  }

  async getStripePortalLink(): Promise<string> {
    const response = await this.httpClient.post<string>(
      `${quizMeConfig.apiEndpointUrl}/${this.baseURL}/stripePortal`,
      {
        headers: {
          Accept: "application/json",
        },
      }
    );

    return response.data;
  }
}
