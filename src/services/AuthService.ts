import HttpClientService from "./HttpClientService";
import { quizMeConfig } from "../config";
import { BaseStorageService } from "./BaseStorageService";
import { storageKeys } from "../models/StorageKeys";
import {
  AuthenticationResultModel,
  ResetPasswordModel,
} from "../models/AuthenticationResultModel";
import { DateTime } from "luxon";

export class AuthService {
  private httpClient: HttpClientService;

  private get storageService(): BaseStorageService | any {
    return quizMeConfig.storageService;
  }

  private static _instance: AuthService;

  public static get Instance(): AuthService {
    return AuthService._instance || (AuthService._instance = new AuthService());
  }

  constructor() {
    if (
      undefined === quizMeConfig.storageService ||
      null === quizMeConfig.storageService
    ) {
      throw new Error(
        "quizMeConfig.storageService must be registered before application starts"
      );
    }

    this.httpClient = HttpClientService.Instance;
  }

  async isLoggedIn(): Promise<boolean> {
    return this.storageService.contains(storageKeys.authorizationToken);
  }

  async signUp(signUpUserModel: any): Promise<AuthenticationResultModel> {
    try {
      const response: AuthenticationResultModel = await this.httpClient
        .post<AuthenticationResultModel>(
          `${quizMeConfig.apiEndpointUrl}/api/SignUp/register`,
          {
            registerFromMobileApp: true,
            ...signUpUserModel,
          },
          {
            headers: {
              Accept: "application/json",
            },
          }
        )
        .then((_) => _.data);

      await this.storageService.store(
        storageKeys.refreshToken,
        response.refreshToken
      );
      return response;
    } catch (error) {
      throw {
        status: error.response.status,
        data: error.response.data,
        response: error.response,
      };
    }
  }

  async requestResetPassword(userEmail: string): Promise<any> {
    try {
      const response: any = await this.httpClient
        .post<any>(
          `${quizMeConfig.apiEndpointUrl}/api/userPassword/requestForgotPasswordEmail`,
          {
            userEmail,
          },
          {
            headers: {
              Accept: "application/json",
            },
          }
        )
        .then((_) => _.data);
    } catch (error) {
      throw {
        status: error.response.status,
        data: error.response.data,
        response: error.response,
      };
    }
  }

  async requestResetPasswordToken(): Promise<ResetPasswordModel> {
    try {
      const response: ResetPasswordModel = await this.httpClient
        .post<ResetPasswordModel>(
          `${quizMeConfig.apiEndpointUrl}/api/userPassword/requestChangePassword`,
          null,
          {
            headers: {
              Accept: "application/json",
            },
          }
        )
        .then((_) => _.data);

      return response;
    } catch (error) {
      throw {
        status: error.response.status,
        data: error.response.data,
        response: error.response,
      };
    }
  }

  async resetPassword(resetPasswordModel: ResetPasswordModel): Promise<any> {
    try {
      const response: any = await this.httpClient
        .post<any>(
          `${quizMeConfig.apiEndpointUrl}/api/userPassword/resetPassword`,
          {
            ...resetPasswordModel,
          },
          {
            headers: {
              Accept: "application/json",
            },
          }
        )
        .then((_) => _.data);

      return response;
    } catch (error) {
      throw {
        status: error.response.status,
        data: error.response.data,
        response: error.response,
      };
    }
  }

  async hasRefreshToken(): Promise<boolean> {
    return await this.storageService.contains(storageKeys.refreshToken);
  }

  async refreshAuthToken(
    timezone: string,
    refreshToken?: string
  ): Promise<AuthenticationResultModel> {
    if (!refreshToken) {
      refreshToken = await this.storageService.retrieve(
        storageKeys.refreshToken
      );
    }

    if (!refreshToken) {
      throw {
        status: 401,
      };
    }

    try {
      const response: AuthenticationResultModel = await this.httpClient
        .post<AuthenticationResultModel>(
          `${quizMeConfig.apiEndpointUrl}/api/Auth/refreshToken`,
          null,
          {
            headers: {
              "X-REFRESH-TOKEN": refreshToken,
              "X-CLIENT-TIMEZONE": timezone,
            },
            ignoreAuthToken: true,
          }
        )
        .then((_) => _.data);
      await this.storageService.store(
        storageKeys.refreshToken,
        response.refreshToken
      );
      return response;
    } catch (error) {
      if (error.response.status === 401) {
        await this.storageService.remove(storageKeys.refreshToken);
      }

      throw {
        status: error.response.status,
        data: error.response.data,
        response: error.response,
      };
    }
  }

  async logIn(
    userName: string,
    password: string
  ): Promise<AuthenticationResultModel> {
    try {
      const response: AuthenticationResultModel = await this.httpClient
        .post<AuthenticationResultModel>(
          `${quizMeConfig.apiEndpointUrl}/api/Auth/login`,
          {
            userName: userName.toLocaleLowerCase(),
            password: password,
            rememberMe: true,
          },
          {
            headers: {
              Accept: "application/json",
              "X-CLIENT-TIMEZONE": DateTime.now().zoneName,
            },
          }
        )
        .then((_) => _.data);

      await this.storageService.store(
        storageKeys.refreshToken,
        response.refreshToken
      );
      return response;
    } catch (error) {
      console.log(JSON.stringify(error));
      if (!error.response) {
        throw error;
      }

      throw {
        status: error.response.status,
        data: error.response.data,
        response: error.response,
      };
    }
  }

  async logout(alreadyLoggedOutFromServer = false): Promise<void> {
    if (!alreadyLoggedOutFromServer) {
      try {
        const response = await this.httpClient
          .post(`${quizMeConfig.apiEndpointUrl}/api/Auth/logout`)
          .then((_) => _.data);
      } catch (error) {
        if (error.response.status === 401) {
          await this.storageService.clear();
          return;
        }
        throw {
          status: error.response.status,
          data: error.response.data,
          response: error.response,
        };
      }
    }

    await this.storageService.clear();
  }

  async loadUserProfile(): Promise<any> {
    try {
      const response = await this.httpClient
        .get(`${quizMeConfig.apiEndpointUrl}/api/Auth/profile`)
        .then((_) => _.data);

      return response;
    } catch (error) {
      throw {
        status: error.response && error.response.status,
        data: error.response && error.response.data,
        response: error.response,
      };
    }
  }
}

export default AuthService;
