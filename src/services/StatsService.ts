import { quizMeConfig } from '../config';
import { HomeDashboardStats } from '../models/HomeDashboardStats';
import HttpClientService from './HttpClientService';

export class StatsService {
  protected httpClient: HttpClientService;
  protected baseURL: string = 'api/stats';

  private static _instance: StatsService;

  static get Instance(): StatsService {
    return StatsService._instance || (StatsService._instance = new StatsService());
  }

  constructor() {
    if (undefined === quizMeConfig.storageService || null === quizMeConfig.storageService) {
      throw new Error('quizMeConfig.storageService must be registered before application starts');
    }
    this.httpClient = HttpClientService.Instance;
  }

  async getHomepageDashboard(): Promise<HomeDashboardStats> {
    const response = await this.httpClient.get<HomeDashboardStats>(
      `${quizMeConfig.apiEndpointUrl}/${this.baseURL}/dashboard`,
      {
        headers: {
          Accept: 'application/json',
        },
      }
    );

    return response.data;
  }
}
