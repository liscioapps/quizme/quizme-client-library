import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';

export interface ExtendedAxiosRequestConfig extends AxiosRequestConfig {
  /**
   * Tells HTTP Clients not to include the Authorization header
   */
  ignoreAuthToken?: boolean;
}

export class HttpClientService {
  static _instance: HttpClientService;
  _apiClient;
  defaultHeaders: any;

  static get Instance(): HttpClientService {
    return HttpClientService._instance || (HttpClientService._instance = new HttpClientService());
  }

  constructor() {
    this._apiClient = axios.create();
    this.defaultHeaders = {
      authorization: null
    }

    this.registerRequestInterceptor((request: any) => {
      if (!request.options || !request.options.ignoreAuthToken) {
        if (this.defaultHeaders.authorization) {
          request.headers['Authorization'] = `Bearer ${this.defaultHeaders.authorization}`;
        }
      }

      return request;
    });

    this.registerResponseInterceptors(
      function (response: any) {
        return response;
      },
      function (error: any) {
        if (!error.response || !error.response.status) {
          throw error;
        }
        else {
          throw {
            status: error.response.status,
            response: error.response,
            data: error.response.data
          };
        }
      });
  }

  setDefaultHeader(name: string, value: any) {
    this.defaultHeaders[name] = value;
  }

  removeDefaultHeader(name: string) {
    this.defaultHeaders[name] = null;
  }

  registerRequestInterceptor(...interceptors: any[]) {
    this._apiClient.interceptors.request.use(...interceptors);
  }

  async registerResponseInterceptors(...interceptors: any[]) {
    this._apiClient.interceptors.response.use(...interceptors);
  }


  /**
   * Make GET request to the given URL
   * @param url The URL to make request
   * @param config the options of the request
   * @returns
   */
  async get<ModelType>(url: string, config?: ExtendedAxiosRequestConfig | undefined): Promise<AxiosResponse<ModelType>> {
    return this._apiClient.get(url, config);
  }

  /**
   * Make POST request to the given URL, optionally include the data
   * @param url The URL to make request
   * @param data Data to include in the POST request
   * @param config the options of the request
   * @returns 
   */
  async post<ModelType>(url: string, data?: any, config?: ExtendedAxiosRequestConfig | undefined): Promise<AxiosResponse<ModelType>> {
    return this._apiClient.post(url, data, config);
  }

  /**
   * Make PUT request to the given URL, optionally include the data
   * @param url The URL to make request
   * @param data Data to include in the POST request
   * @param config the options of the request
   * @returns
   */
  async put<ModelType>(url: string, data?: any, config?: ExtendedAxiosRequestConfig | undefined): Promise<AxiosResponse<ModelType>> {
    return this._apiClient.put(url, data, config);
  }

  /**
   * Make DELETE request to the given URL
   * @param url The URL to make request
   * @param config the options of the request
   * @returns
   */
  async delete<ModelType>(url: string, config?: ExtendedAxiosRequestConfig | undefined) : Promise<AxiosResponse<ModelType>> {
    return this._apiClient.delete(url, config);
  }
}

export default HttpClientService;