import HttpClientService from './HttpClientService';
import { quizMeConfig } from '../config';
import { BaseStorageService } from './BaseStorageService';
import { PaginatedResponse } from '../models/PaginatedResponse';

export abstract class BaseCRUDApiClient<TModel> {
  protected httpClient: HttpClientService;

  protected get storageService(): BaseStorageService | any {
    return quizMeConfig.storageService;
  }

  protected abstract baseURL: string;

  constructor() {
    if (undefined === quizMeConfig.storageService || null === quizMeConfig.storageService) {
      throw new Error('quizMeConfig.storageService must be registered before application starts');
    }

    this.httpClient = HttpClientService.Instance;
  }

  /**
   * Retrieves list of items with pagination support
   * @param pageSize number of items to retrieve in the request
   * @param pageIndex the index of the page to return
   * @param orderBy specifies the property to sort the items before return
   * @param columns the properties list of the entity to return
   * @param otherFilters other filters
   * @returns List of items
   */
  async getList(
    pageSize: number = 20,
    pageIndex: number = 0,
    orderBy: string = '',
    columns: string[] = [],
    otherFilters: any = null
  ): Promise<PaginatedResponse<TModel[]>> {
    const response = await this.httpClient.get<TModel[]>(
      `${quizMeConfig.apiEndpointUrl}/${this.baseURL}`,
      {
        headers: {
          Accept: 'application/json',
        },
        params: {
          pageIndex,
          pageSize,
          orderBy: orderBy || null,
          columns: columns.length ? columns.join(',') : null,
          ...(otherFilters || {}),
        },
      }
    );

    const resultCount = Number(response.headers['result-count']);
    const resultPageIndex = Number(response.headers['result-pageindex']);
    const resultPageSize = Number(response.headers['result-pagesize']);
    const resultTotals = Number(response.headers['result-totals']);
    const didReachEnd = resultTotals <= resultPageIndex * resultPageSize + resultCount;

    return {
      data: response.data,
      resultCount,
      resultPageIndex,
      resultPageSize,
      resultTotals,
      didReachEnd,
    };
  }

  async getItem(itemId: number, columns: string[] = []): Promise<TModel> {
    const response: TModel = await this.httpClient
      .get<TModel>(`${quizMeConfig.apiEndpointUrl}/${this.baseURL}/${itemId}`, {
        headers: {
          Accept: 'application/json',
        },
        params: {
          columns: columns.length ? columns.join(',') : null,
        },
      })
      .then((_) => _.data);

    return response;
  }

  async createItem(item: TModel): Promise<any> {
    const response: any = await this.httpClient
      .post<any>(
        `${quizMeConfig.apiEndpointUrl}/${this.baseURL}`,
        {
          ...item,
        },
        {
          headers: {
            Accept: 'application/json',
          },
        }
      )
      .then((_) => _.data);

    return response;
  }

  async updateItem(itemId: number, item: Partial<TModel>): Promise<any> {
    const response: any = await this.httpClient
      .put<any>(
        `${quizMeConfig.apiEndpointUrl}/${this.baseURL}/${itemId}`,
        {
          ...item,
        },
        {
          headers: {
            Accept: 'application/json',
          },
        }
      )
      .then((_) => _.data);

    return response;
  }

  async deleteItem(itemId: number): Promise<any> {
    const response: any = await this.httpClient
      .delete<any>(`${quizMeConfig.apiEndpointUrl}/${this.baseURL}/${itemId}`, {
        headers: {
          Accept: 'application/json',
        },
      })
      .then((_) => _.data);

    return response;
  }
}
