import { quizMeConfig } from '../config';
import HttpClientService from './HttpClientService';

export class UserEventsService {
  protected httpClient: HttpClientService;
  protected baseURL: string = 'api/userEvents';

  private static _instance: UserEventsService;

  static get Instance(): UserEventsService {
    return UserEventsService._instance || (UserEventsService._instance = new UserEventsService());
  }

  constructor() {
    if (undefined === quizMeConfig.storageService || null === quizMeConfig.storageService) {
      throw new Error('quizMeConfig.storageService must be registered before application starts');
    }
    this.httpClient = HttpClientService.Instance;
  }

  async getOnboardCompleteStatus(): Promise<boolean> {
    const response = await this.httpClient.get<boolean>(
      `${quizMeConfig.apiEndpointUrl}/${this.baseURL}/onboardStatus`,
      {
        headers: {
          Accept: 'application/json',
        },
      }
    );

    return response.data;
  }

  async setOnboardCompleteStatus(agent: 'web' | 'mobile'): Promise<void> {
    await this.httpClient.post<boolean>(
      `${quizMeConfig.apiEndpointUrl}/${this.baseURL}/onboardComplete/${agent}`,
      {
        headers: {
          Accept: 'application/json',
        },
      }
    );
  }
}
