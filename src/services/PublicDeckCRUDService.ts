import { BaseCRUDApiClient } from "./BaseCRUDApiClient";
import { quizMeConfig } from "../config";
import { PublicDeckModel } from "../models/PublicDeckModel";

export class PublicDeckCRUDService extends BaseCRUDApiClient<PublicDeckModel> {
  protected baseURL: string = "api/PublicDecks";

  private static _instance: PublicDeckCRUDService;

  static get Instance(): PublicDeckCRUDService {
    return (
      PublicDeckCRUDService._instance ||
      (PublicDeckCRUDService._instance = new PublicDeckCRUDService())
    );
  }

  async addToMyDecks(publicDeckId: number): Promise<number> {
    const response = await this.httpClient
      .post<number>(
        `${quizMeConfig.apiEndpointUrl}/${this.baseURL}/${publicDeckId}/addToMyDecks`,
        {
          headers: {
            Accept: "application/json",
          },
        }
      )
      .then((_) => _.data);

    return response;
  }

  async requestPublish(deckId: number): Promise<number> {
    const response = await this.httpClient
      .post<number>(
        `${quizMeConfig.apiEndpointUrl}/${this.baseURL}/requestPublish`,
        { originalDeckId: deckId },
        {
          headers: {
            Accept: "application/json",
          },
        }
      )
      .then((_) => _.data);

    return response;
  }

  async approveDeck(publicDeckId: number): Promise<number> {
    const response = await this.httpClient
      .post<number>(
        `${quizMeConfig.apiEndpointUrl}/${this.baseURL}/${publicDeckId}/approve`,
        {
          headers: {
            Accept: "application/json",
          },
        }
      )
      .then((_) => _.data);

    return response;
  }

  async unapproveDeck(publicDeckId: number): Promise<number> {
    const response = await this.httpClient
      .post<number>(
        `${quizMeConfig.apiEndpointUrl}/${this.baseURL}/${publicDeckId}/unapprove`,
        {
          headers: {
            Accept: "application/json",
          },
        }
      )
      .then((_) => _.data);

    return response;
  }
}
