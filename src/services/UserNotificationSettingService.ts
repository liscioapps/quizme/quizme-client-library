import { quizMeConfig } from "../config";
import { UserNotificationSettingModel } from "../models/UserNotificationSettingModel";
import HttpClientService from "./HttpClientService";

export class UserNotificationSettingService {
  protected httpClient: HttpClientService;
  protected baseURL: string = "api/userNotificationSetting";

  private static _instance: UserNotificationSettingService;

  static get Instance(): UserNotificationSettingService {
    return (
      UserNotificationSettingService._instance ||
      (UserNotificationSettingService._instance =
        new UserNotificationSettingService())
    );
  }

  constructor() {
    if (
      undefined === quizMeConfig.storageService ||
      null === quizMeConfig.storageService
    ) {
      throw new Error(
        "quizMeConfig.storageService must be registered before application starts"
      );
    }
    this.httpClient = HttpClientService.Instance;
  }

  async getUserDueDeckNotificationSettings(): Promise<UserNotificationSettingModel> {
    const response = await this.httpClient.get<UserNotificationSettingModel>(
      `${quizMeConfig.apiEndpointUrl}/${this.baseURL}/dueDeckNotification`,
      {
        headers: {
          Accept: "application/json",
        },
      }
    );

    return response.data;
  }

  async updateUserDueDeckNotificationSettings(
    settings: UserNotificationSettingModel
  ): Promise<void> {
    await this.httpClient.post(
      `${quizMeConfig.apiEndpointUrl}/${this.baseURL}/dueDeckNotification`,
      settings,
      {
        headers: {
          Accept: "application/json",
        },
      }
    );
  }
}
