import { nanoid } from '@reduxjs/toolkit';
import { PublicDeckModel } from '../models/PublicDeckModel';
import { PublicDeckSummaryModel } from '../models/PublicDeckSummaryModel';

const PUBLIC_DECKS = '[PUBLIC_DECKLIST]';

//#region event actions
interface FetchPublicDecksSucceded {
  type: `${typeof PUBLIC_DECKS} FETCH_DECKS_SUCCEDED`;
  payload: PublicDeckSummaryModel[];
  meta: {
    context: 'initial' | 'nextPage';
    newPage: number;
    totalRecords: number;
    reachedEnd: boolean;
  };
}

export const fetchPublicDecksSucceded = (
  decks: PublicDeckSummaryModel[],
  newPage: number,
  totalRecords: number,
  reachedEnd: boolean,
  context: 'initial' | 'nextPage'
): FetchPublicDecksSucceded => {
  return {
    type: '[PUBLIC_DECKLIST] FETCH_DECKS_SUCCEDED',
    payload: decks,
    meta: {
      context,
      newPage,
      totalRecords,
      reachedEnd,
    },
  };
};

interface FetchPublicDecksFailed {
  type: `${typeof PUBLIC_DECKS} FETCH_DECKS_FAILURE`;
  payload: {
    errorMsg: string;
  };
}

export const fetchPublicDecksFailed = (errorMessage: string): FetchPublicDecksFailed => {
  return {
    type: '[PUBLIC_DECKLIST] FETCH_DECKS_FAILURE',
    payload: {
      errorMsg: errorMessage,
    },
  };
};

interface PublicSearchTermChanged {
  type: `${typeof PUBLIC_DECKS} SEARCH_TERM_CHANGED`;
  payload: string;
}

export const publicSearchTermChanged = (searchTerm: string): PublicSearchTermChanged => {
  return {
    type: '[PUBLIC_DECKLIST] SEARCH_TERM_CHANGED',
    payload: searchTerm,
  };
};

//# endregion

//#region command actions

interface FetchInitialPublicDeckListAction {
  type: `${typeof PUBLIC_DECKS} INITIAL_FETCH_LIST`;
}

export const fetchInitialPublicDeckList = (): FetchInitialPublicDeckListAction => ({
  type: '[PUBLIC_DECKLIST] INITIAL_FETCH_LIST',
});

interface FetchNextPublicDeckListPage {
  type: `${typeof PUBLIC_DECKS} FETCH_NEXT_LIST_PAGE`;
}

export const fetchNextPublicDeckListPage = (): FetchNextPublicDeckListPage => ({
  type: '[PUBLIC_DECKLIST] FETCH_NEXT_LIST_PAGE',
});

//#endregion

//#region document actions

interface SetPublicPageSize {
  type: `${typeof PUBLIC_DECKS} SET_PAGE_SIZE`;
  payload: number;
}

export const setPublicPageSize = (pageSize: number): SetPublicPageSize => {
  return {
    type: '[PUBLIC_DECKLIST] SET_PAGE_SIZE',
    payload: pageSize,
  };
};

interface IncrementPublicPageIndex {
  type: `${typeof PUBLIC_DECKS} INCREMENT_PAGE_INDEX`;
}

export const setPublicPageIndex = (): IncrementPublicPageIndex => {
  return {
    type: '[PUBLIC_DECKLIST] INCREMENT_PAGE_INDEX',
  };
};

interface SetPublicDecks {
  type: `${typeof PUBLIC_DECKS} SET_DECKS`;
  payload: PublicDeckSummaryModel[];
}

export const setPublicDecks = (decks: PublicDeckSummaryModel[]): SetPublicDecks => ({
  type: `[PUBLIC_DECKLIST] SET_DECKS`,
  payload: decks,
});

interface AppendPublicDecks {
  type: `${typeof PUBLIC_DECKS} APPEND_DECKS`;
  payload: PublicDeckSummaryModel[];
}

export const appendPublicDecks = (decks: PublicDeckSummaryModel[]): AppendPublicDecks => ({
  type: '[PUBLIC_DECKLIST] APPEND_DECKS',
  payload: decks,
});

interface SetPublicDeckPagination {
  type: `${typeof PUBLIC_DECKS} SET_PAGINATION`;
  payload: {
    currentPage: number;
    totalRecords: number;
    didReachEnd: boolean;
  };
}

export const setPublicDeckPagination = (
  currentPage: number,
  totalRecords: number,
  didReachEnd: boolean
): SetPublicDeckPagination => {
  return {
    type: '[PUBLIC_DECKLIST] SET_PAGINATION',
    payload: {
      currentPage,
      totalRecords,
      didReachEnd,
    },
  };
};

interface SetPublicDeckListFetching {
  type: `${typeof PUBLIC_DECKS} SET_FETCHING`;
  payload: boolean;
}

export const setPublicDeckListFetching = (loading: boolean): SetPublicDeckListFetching => {
  return {
    type: '[PUBLIC_DECKLIST] SET_FETCHING',
    payload: loading,
  };
};

interface SetPublicDeckListHasInitialLoad {
  type: `${typeof PUBLIC_DECKS} SET_HAS_INITIAL_LOAD`;
  payload: boolean;
}

export const setPublicDeckListHasInitialLoad = (
  hasInitialLoad: boolean
): SetPublicDeckListHasInitialLoad => {
  return {
    type: '[PUBLIC_DECKLIST] SET_HAS_INITIAL_LOAD',
    payload: hasInitialLoad,
  };
};

interface SetPublicDeckListNotification {
  type: `${typeof PUBLIC_DECKS} SET_NOTIFICATION`;
  payload: {
    message: string;
    id: string;
  };
}

export const setPublicDeckListNotification = (message: string): SetPublicDeckListNotification => {
  return {
    type: '[PUBLIC_DECKLIST] SET_NOTIFICATION',
    payload: {
      message,
      id: nanoid(),
    },
  };
};

interface RemovePublicDeckListNotification {
  type: `${typeof PUBLIC_DECKS} REMOVE_NOTIFICATION`;
  payload: {
    notificationId: string;
  };
}

export const removePubilcDeckListNotification = (
  notificationId: string
): RemovePublicDeckListNotification => {
  return {
    type: '[PUBLIC_DECKLIST] REMOVE_NOTIFICATION',
    payload: {
      notificationId,
    },
  };
};

//#endregion

export type PublicDeckActions =
  | FetchInitialPublicDeckListAction
  | FetchNextPublicDeckListPage
  | SetPublicDecks
  | AppendPublicDecks
  | FetchPublicDecksFailed
  | FetchPublicDecksSucceded
  | SetPublicPageSize
  | IncrementPublicPageIndex
  | SetPublicDeckPagination
  | PublicSearchTermChanged
  | SetPublicDeckListFetching
  | SetPublicDeckListHasInitialLoad
  | PublicSearchTermChanged;

export type PublicDeckActionTypes = PublicDeckActions['type'];
