import { PublicDeckModel } from "../models/PublicDeckModel";

const PUBLIC_DECKS = "[SELECTED_PUBLIC_DECK]";

//#region event actions
interface FetchPublicDeckSucceded {
  type: `${typeof PUBLIC_DECKS} FETCH_SUCCEDED`;
  payload: PublicDeckModel;
}

export const fetchSelectedPublicDeckSucceeded = (
  deck: PublicDeckModel
): FetchPublicDeckSucceded => {
  return {
    type: "[SELECTED_PUBLIC_DECK] FETCH_SUCCEDED",
    payload: deck,
  };
};

interface FetchPublicDeckFailed {
  type: `${typeof PUBLIC_DECKS} FETCH_DECK_FAILURE`;
  payload: {
    errorMsg: string;
  };
}

export const fetchSelectedPublicDeckFailed = (
  errorMessage: string
): FetchPublicDeckFailed => {
  return {
    type: "[SELECTED_PUBLIC_DECK] FETCH_DECK_FAILURE",
    payload: {
      errorMsg: errorMessage,
    },
  };
};

interface AddToMyDecksFailed {
  type: `${typeof PUBLIC_DECKS} ADD_TO_MY_DECKS_FAILED`;
  payload: {
    errorMsg: string;
  };
}

export const addToMyDecksFailed = (
  errorMessage: string
): AddToMyDecksFailed => {
  return {
    type: "[SELECTED_PUBLIC_DECK] ADD_TO_MY_DECKS_FAILED",
    payload: {
      errorMsg: errorMessage,
    },
  };
};

interface AddToMyDecksSucceeded {
  type: `${typeof PUBLIC_DECKS} ADD_TO_MY_DECKS_SUCCEEDED`;
  payload: {
    privateDeckId: number;
  };
}

export const addToMyDecksSucceeded = (
  privateDeckId: number
): AddToMyDecksSucceeded => {
  return {
    type: "[SELECTED_PUBLIC_DECK] ADD_TO_MY_DECKS_SUCCEEDED",
    payload: {
      privateDeckId,
    },
  };
};

//# endregion

//#region command actions

interface FetchPublicDeck {
  type: `${typeof PUBLIC_DECKS} FETCH_PUBLIC_DECK`;
  payload: {
    publicDeckId: number;
  };
}

export const fetchSelectedPublicDeck = (
  publicDeckId: number
): FetchPublicDeck => ({
  type: "[SELECTED_PUBLIC_DECK] FETCH_PUBLIC_DECK",
  payload: {
    publicDeckId,
  },
});

interface AddPublicDeckToMyDecks {
  type: `${typeof PUBLIC_DECKS} ADD_TO_MY_DECKS`;
  payload: {
    publicDeckId: number;
  };
}

export const addPublicDeckToMyDecks = (
  publicDeckId: number
): AddPublicDeckToMyDecks => ({
  type: "[SELECTED_PUBLIC_DECK] ADD_TO_MY_DECKS",
  payload: {
    publicDeckId,
  },
});

//#endregion

//#region document actions

interface SetSelectedPublicDeck {
  type: `${typeof PUBLIC_DECKS} SET_DECK`;
  payload: {
    deck: PublicDeckModel;
  };
}

export const setSelectedPublicDeck = (
  deck: PublicDeckModel
): SetSelectedPublicDeck => {
  return {
    type: "[SELECTED_PUBLIC_DECK] SET_DECK",
    payload: {
      deck,
    },
  };
};

interface SetSelectePublicDeckPrivateDeckId {
  type: `${typeof PUBLIC_DECKS} SET_PRIVATE_DECK_ID`;
  payload: {
    privateDeckId?: number;
  };
}

export const setSelectePublicDeckPrivateDeckId = (
  privateDeckId?: number
): SetSelectePublicDeckPrivateDeckId => {
  return {
    type: "[SELECTED_PUBLIC_DECK] SET_PRIVATE_DECK_ID",
    payload: {
      privateDeckId,
    },
  };
};

interface SetSelectedPublicDeckIsFetching {
  type: `${typeof PUBLIC_DECKS} SET_IS_FETCHING`;
  payload: {
    isFetching: boolean;
  };
}

export const SetSelectedPublicDeckIsFetching = (
  isFetching: boolean
): SetSelectedPublicDeckIsFetching => {
  return {
    type: "[SELECTED_PUBLIC_DECK] SET_IS_FETCHING",
    payload: {
      isFetching,
    },
  };
};

interface SetSelectedPublicDeckError {
  type: `${typeof PUBLIC_DECKS} SET_ERROR`;
  payload: {
    errorMessage: string;
  };
}

export const setSelectedPublicDeckError = (
  errorMessage: string
): SetSelectedPublicDeckError => {
  return {
    type: "[SELECTED_PUBLIC_DECK] SET_ERROR",
    payload: {
      errorMessage,
    },
  };
};

interface ClearSelectedPublicDeckError {
  type: `${typeof PUBLIC_DECKS} CLEAR_ERROR`;
}

export const clearSelectedPublicDeckError =
  (): ClearSelectedPublicDeckError => {
    return {
      type: "[SELECTED_PUBLIC_DECK] CLEAR_ERROR",
    };
  };

// approve decks

interface RequestApproveSelectedPublicDeck {
  type: `${typeof PUBLIC_DECKS} REQUEST APPROVE`;
}

export const requestApproveSelectedPublicDeck =
  (): RequestApproveSelectedPublicDeck => {
    return {
      type: "[SELECTED_PUBLIC_DECK] REQUEST APPROVE",
    };
  };

interface RequestUnapproveSelectedPublicDeck {
  type: `${typeof PUBLIC_DECKS} REQUEST UNAPPROVE`;
}

export const requestUnapproveSelectedPublicDeck =
  (): RequestUnapproveSelectedPublicDeck => {
    return {
      type: "[SELECTED_PUBLIC_DECK] REQUEST UNAPPROVE",
    };
  };

interface SetSelectedPublicDeckApproved {
  type: `${typeof PUBLIC_DECKS} SET APPROVED`;
  payload: {
    approved: boolean;
  };
}

export const setSelectedPublicDeckApproved = (
  approved: boolean
): SetSelectedPublicDeckApproved => {
  return {
    type: "[SELECTED_PUBLIC_DECK] SET APPROVED",
    payload: {
      approved,
    },
  };
};

interface RequestApprovalChangeFailed {
  type: `${typeof PUBLIC_DECKS} REQUEST APPROVAL CHANGE FAILED`;
  payload: {
    message: string;
  };
}

export const requestApprovalChangeFailed = (
  message: string = "Error updating approval status"
): RequestApprovalChangeFailed => {
  return {
    type: "[SELECTED_PUBLIC_DECK] REQUEST APPROVAL CHANGE FAILED",
    payload: {
      message: message,
    },
  };
};

//#endregion

export type SelectedPublicDeckActions =
  | FetchPublicDeckSucceded
  | FetchPublicDeckFailed
  | FetchPublicDeck
  | AddPublicDeckToMyDecks
  | SetSelectedPublicDeck
  | SetSelectedPublicDeckIsFetching
  | SetSelectedPublicDeckError
  | ClearSelectedPublicDeckError
  | SetSelectePublicDeckPrivateDeckId
  | AddToMyDecksFailed
  | AddToMyDecksSucceeded
  | RequestApproveSelectedPublicDeck
  | RequestUnapproveSelectedPublicDeck
  | SetSelectedPublicDeckApproved
  | RequestApprovalChangeFailed;

export type SelectedPublicDeckActionsTypes = SelectedPublicDeckActions["type"];
