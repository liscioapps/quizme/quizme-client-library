import { nanoid } from "@reduxjs/toolkit";
import { DeckModel, DeckSummaryModel } from "../models";

const DECKLIST = "[DECKLIST]";

//#region event actions
interface FetchDecksSucceded {
  type: `${typeof DECKLIST} FETCH_DECKS_SUCCEDED`;
  payload: DeckSummaryModel[];
  meta: {
    context: "initial" | "nextPage";
    newPage: number;
    totalRecords: number;
    reachedEnd: boolean;
  };
}

export const fetchDecksSucceded = (
  decks: DeckSummaryModel[],
  newPage: number,
  totalRecords: number,
  reachedEnd: boolean,
  context: "initial" | "nextPage"
): FetchDecksSucceded => {
  return {
    type: "[DECKLIST] FETCH_DECKS_SUCCEDED",
    payload: decks,
    meta: {
      context,
      newPage,
      totalRecords,
      reachedEnd,
    },
  };
};

interface FetchDecksFailed {
  type: `${typeof DECKLIST} FETCH_DECKS_FAILURE`;
  payload: {
    errorMsg: string;
  };
}

export const fetchDecksFailed = (errorMessage: string): FetchDecksFailed => {
  return {
    type: "[DECKLIST] FETCH_DECKS_FAILURE",
    payload: {
      errorMsg: errorMessage,
    },
  };
};

interface SortPropertiesChanged {
  type: `${typeof DECKLIST} SORT_PROPERTIES_CHANGED`;
  payload: {
    field: string;
    direction: "asc" | "desc";
  };
}

export const sortPropertiesChanged = (
  field: string,
  direction: "asc" | "desc"
): SortPropertiesChanged => {
  return {
    type: "[DECKLIST] SORT_PROPERTIES_CHANGED",
    payload: {
      field,
      direction,
    },
  };
};

interface SearchTermChanged {
  type: `${typeof DECKLIST} SEARCH_TERM_CHANGED`;
  payload: string;
}

export const searchTermChanged = (searchTerm: string): SearchTermChanged => {
  return {
    type: "[DECKLIST] SEARCH_TERM_CHANGED",
    payload: searchTerm,
  };
};

//# endregion

//#region command actions

interface FetchInitialDeckListAction {
  type: `${typeof DECKLIST} INITIAL_FETCH_LIST`;
}

export const fetchInitialDeckList = (): FetchInitialDeckListAction => ({
  type: "[DECKLIST] INITIAL_FETCH_LIST",
});

interface FetchNextDeckListPage {
  type: `${typeof DECKLIST} FETCH_NEXT_LIST_PAGE`;
}

export const fetchNextDeckListPage = (): FetchNextDeckListPage => ({
  type: "[DECKLIST] FETCH_NEXT_LIST_PAGE",
});

//#endregion

//#region document actions

interface SetPageSize {
  type: `${typeof DECKLIST} SET_PAGE_SIZE`;
  payload: number;
}

export const setPageSize = (pageSize: number): SetPageSize => {
  return {
    type: "[DECKLIST] SET_PAGE_SIZE",
    payload: pageSize,
  };
};

interface IncrementPageIndex {
  type: `${typeof DECKLIST} INCREMENT_PAGE_INDEX`;
}

export const setPageIndex = (): IncrementPageIndex => {
  return {
    type: "[DECKLIST] INCREMENT_PAGE_INDEX",
  };
};

interface SetOrderByAscending {
  type: `${typeof DECKLIST} SET_ORDER_BY_ASCENDING`;
  payload: {
    field: string;
  };
}

export const setOrderByAscending = (field: string): SetOrderByAscending => {
  return {
    type: "[DECKLIST] SET_ORDER_BY_ASCENDING",
    payload: {
      field,
    },
  };
};

interface SetOrderByDescending {
  type: `${typeof DECKLIST} SET_ORDER_BY_DESCENDING`;
  payload: {
    field: string;
  };
}

export const setOrderByDescending = (field: string): SetOrderByDescending => {
  return {
    type: "[DECKLIST] SET_ORDER_BY_DESCENDING",
    payload: {
      field,
    },
  };
};

interface SetDecks {
  type: `${typeof DECKLIST} SET_DECKS`;
  payload: DeckSummaryModel[];
}

export const setDecks = (decks: DeckModel[]): SetDecks => ({
  type: `[DECKLIST] SET_DECKS`,
  payload: decks,
});

interface AppendDecks {
  type: `${typeof DECKLIST} APPEND_DECKS`;
  payload: DeckSummaryModel[];
}

export const appendDecks = (decks: DeckModel[]): AppendDecks => ({
  type: "[DECKLIST] APPEND_DECKS",
  payload: decks,
});

interface SetPagination {
  type: `${typeof DECKLIST} SET_PAGINATION`;
  payload: {
    currentPage: number;
    totalRecords: number;
    didReachEnd: boolean;
  };
}

export const setPagination = (
  currentPage: number,
  totalRecords: number,
  didReachEnd: boolean
): SetPagination => {
  return {
    type: "[DECKLIST] SET_PAGINATION",
    payload: {
      currentPage,
      totalRecords,
      didReachEnd,
    },
  };
};

interface SetDeckListFetching {
  type: `${typeof DECKLIST} SET_FETCHING`;
  payload: boolean;
}

export const setDeckListFetching = (loading: boolean): SetDeckListFetching => {
  return {
    type: "[DECKLIST] SET_FETCHING",
    payload: loading,
  };
};

interface SetHasInitialLoad {
  type: `${typeof DECKLIST} SET_HAS_INITIAL_LOAD`;
  payload: boolean;
}

export const setDeckListHasInitialLoad = (
  hasInitialLoad: boolean
): SetHasInitialLoad => {
  return {
    type: "[DECKLIST] SET_HAS_INITIAL_LOAD",
    payload: hasInitialLoad,
  };
};

interface SetNotification {
  type: `${typeof DECKLIST} SET_NOTIFICATION`;
  payload: {
    message: string;
    id: string;
  };
}

export const setNotification = (message: string): SetNotification => {
  return {
    type: "[DECKLIST] SET_NOTIFICATION",
    payload: {
      message,
      id: nanoid(),
    },
  };
};

interface RemoveNotification {
  type: `${typeof DECKLIST} REMOVE_NOTIFICATION`;
  payload: {
    notificationId: string;
  };
}

export const removeNotification = (
  notificationId: string
): RemoveNotification => {
  return {
    type: "[DECKLIST] REMOVE_NOTIFICATION",
    payload: {
      notificationId,
    },
  };
};

//#endregion

export type DeckActions =
  | FetchInitialDeckListAction
  | FetchNextDeckListPage
  | SetDecks
  | AppendDecks
  | FetchDecksSucceded
  | FetchDecksFailed
  | SetPageSize
  | IncrementPageIndex
  | SetOrderByAscending
  | SetOrderByDescending
  | SetPagination
  | SortPropertiesChanged
  | SearchTermChanged
  | SetDeckListFetching
  | SetHasInitialLoad;

export type DeckActionTypes = DeckActions["type"];
