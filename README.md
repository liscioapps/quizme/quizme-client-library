# Quizme Client Library

This project is used to share code between React Web App and React Native Mobile App.

## Installation to your project

1. Create a `.npmrc` file at the root of your project, and add the following line into it.

```txt
registry = [your-private-npm-registry-url]
```

2. Install the package as usual

```bash
npm install @liscioapps/quizme-client-library --save
```

## Development

### Commiting with semantic-release

This package uses [semantic-relase](https://github.com/semantic-release/semantic-release) as described in the [GitLab documentation](https://docs.gitlab.com/ee/ci/examples/semantic-release.html) to automatically increment releases based on the commits made, and push those releases to the [GitLab Package Registry](https://docs.gitlab.com/ee/user/packages/).

This means that commit messages have to conform to [these rules](https://semantic-release.gitbook.io/semantic-release/#commit-message-format):

| Commit message                                                                                                                                                                                   | Release type                                                                                                    |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | --------------------------------------------------------------------------------------------------------------- |
| `fix(pencil): stop graphite breaking when too much pressure applied`                                                                                                                             | ~~Patch~~ Fix Release                                                                                           |
| `feat(pencil): add 'graphiteWidth' option`                                                                                                                                                       | ~~Minor~~ Feature Release                                                                                       |
| `perf(pencil): remove graphiteWidth option`<br><br>`BREAKING CHANGE: The graphiteWidth option has been removed.`<br>`The default graphite width of 10mm is always used for performance reasons.` | ~~Major~~ Breaking Release <br /> (Note that the `BREAKING CHANGE: ` token must be in the footer of the commit) |

The list of acceptable types is:

- build
- chore
- ci
- docs
- feat
- fix
- perf
- refactor
- revert
- style
- test

To commit _without_ triggering any release, use the `chore:` keyword.

### Pre-commit hooks

To help us remember to use those commit messages, we've installed pre-commit hooks to this repository. This uses two tools - [commitlint](https://commitlint.js.org/#/) to control the linting of commit messages and [husky](https://typicode.github.io/husky/#/) to run the pre-commit hooks in a nice way.

## Manually Publishing

just don't

### Publish the package to private NPM registry

```bash

npm publish --registry <your-private-npm-registry>

```

### force build

sorry not sorry
